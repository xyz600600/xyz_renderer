# -*- coding: utf-8 -*-

def output_single(fout, y, x, height, width):
    fout.write("v {} {} {}\n".format(x, y, 0))
    fout.write("v {} {} {}\n".format(x + width, y, 0))
    fout.write("v {} {} {}\n".format(x + width, y + height, 0))
    fout.write("v {} {} {}\n".format(x, y + height, 0))
    fout.write("f -4 -3 -2 -1\n\n")

size = 100

sx = 0.0
ex = 1.0
sy = 0.0
ey = 1.0
width = (ex - sx) / size
height = (ey - sy) / size

fout = open('test.obj', 'w')

fout.write("mtllib test.mtl\n")
fout.write("usemtl test\n\n")

for i in range(size):
    for j in range(size):
        y = sy + (ey - sy) * i / size
        x = sx + (ex - sx) * j / size
        output_single(fout, y, x, height, width)