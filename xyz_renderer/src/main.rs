#![feature(bufreader_buffer)] 
#![feature(test)]
#![feature(ptr_internals)]
#![feature(extern_prelude)]

mod common;
mod ray;
mod math;
mod image;
mod random;
mod brdf;
mod texture;
mod object;
mod camera;
mod config;
mod object_manager;
mod environment;
mod render;
mod util;

extern crate rayon;
extern crate test;
extern crate x86intrin;
extern crate rand;

use brdf::ReflectionType::{Diffuse, Specular, Refraction};
use environment::Environment;
use render::{render, render_parallel, render_parallel_prod};
use object::{Material, Intersectable, Object, Primitive};
use object::model_object::ModelObject;
use object::sphere::Sphere;
use object::rectangle::{AABB, Rectangle};
use object_manager::linear_object_manager::LinearObjectManager;
use texture::monotone_texture::MonotoneTexture;
use math::vec3::{Vec3, Color};
use config::Config;
use camera::pinhole::PinHoleCamera;
use camera::thin_lense::ThinLenseCamera;
use camera::Camera;
use random::Random;

use rand::random;

use std::time::Instant;

fn get_environment_sample() -> Environment {

    let time = Instant::now();

    let mut obj_manager: Vec<Box<Object>> = vec![];

    let rads = [1e5f64, 1e5f64, 1e5f64, 1e5f64, 1e5f64, 1e5f64, 20f64, 16.5f64, 16.5f64, 5f64];
    let centers = [
        Vec3::new(1e5f64 + 1f64, 40.8f64, 81.6f64),
        Vec3::new(-1e5f64 + 99f64, 40.8f64, 81.6f64),
        Vec3::new(50f64, 40.8f64, 1e5f64),
        Vec3::new(50f64, 40.8f64, -1e5f64 + 250f64),
        Vec3::new(50f64, 1e5f64, 81.6f64),
        Vec3::new(50f64, -1e5f64 + 100f64, 81.6f64),
        Vec3::new(65f64, 20f64, 20f64),
        Vec3::new(27f64, 16.5f64, 47f64),
        Vec3::new(77f64, 16.5f64, 78f64),
        Vec3::new(50f64, 70f64, 81.6f64)
    ];
    let emits = [
        Color::new(0f64, 0f64, 0f64), 
        Color::new(0f64, 0f64, 0f64), 
        Color::new(0f64, 0f64, 0f64), 
        Color::new(0f64, 0f64, 0f64), 
        Color::new(0f64, 0f64, 0f64), 
        Color::new(0f64, 0f64, 0f64), 
        Color::new(0f64, 0f64, 0f64), 
        Color::new(0f64, 0f64, 0f64), 
        Color::new(0f64, 0f64, 0f64), 
        Color::new(36f64, 36f64, 36f64)
    ];
    let texture_colors = [
        Color::new(0.75f64, 0.25f64, 0.25f64), 
        Color::new(0.25f64, 0.25f64, 0.75f64), 
        Color::new(0.75f64, 0.75f64, 0.75f64), 
        Color::new(0f64, 0f64, 0f64), 
        Color::new(0.75f64, 0.75f64, 0.75f64), 
        Color::new(0.75f64, 0.75f64, 0.75f64), 
        Color::new(0.25f64, 0.75f64, 0.25f64), 
        Color::new(0.99f64, 0.99f64, 0.99f64), 
        Color::new(0.99f64, 0.99f64, 0.99f64), 
        Color::new(0f64, 0f64, 0f64)
    ];

    let reflection_types = [
        Diffuse,
        Diffuse,
        Diffuse,
        Diffuse,
        Diffuse,
        Diffuse,
        Diffuse,
        Specular,
        Refraction(1.5),
        Diffuse,
    ];

    let mut config = Config::new();

    let mut major_id = 0u64;

    for i in 0..10 {
        let obj = Box::new(Sphere::new(centers[i], rads[i], Material::new(Box::new(MonotoneTexture::new(texture_colors[i])), reflection_types[i], emits[i])));
        obj_manager.push(obj);
        major_id += 1;
    }

    config.sample_per_pixel = 32;
    config.grid_size = 16;
    config.screen_resolution_x = 640;
    config.screen_resolution_y = 480;
    config.camera_pos = Vec3::new(50f64, 22f64, 220f64);
    config.camera_dir = Vec3::new(0f64, -0.04f64, -1.0f64).normalize();
    config.camera_up = Vec3::new(0f64, 1f64, 0f64);
    let scale = 1f64;
    config.screen_width = scale * 30f64 * (config.screen_resolution_x as f64 / config.screen_resolution_y as f64);
    config.screen_height = scale * 30f64;
    config.camera_dist = scale * 40f64;
    config.output_path = "result.png".to_string();
    config.lense_radius = scale * 2f64;
    config.lense_focal = 26.6f64;

    let camera = Box::new(PinHoleCamera::new(&config));
    // let camera = Box::new(ThinLenseCamera::new(&config));

    let mut env = Environment::new(camera, config, time);
    env.push(obj_manager);
    env
}

fn get_environment() -> Environment {

    let time = Instant::now();

    let mut config = Config::new();

    let mut obj_array: Vec<Box<Object>> = vec![];

    let scale = 8f64;
    // 白線
    for x in 0..5 {
        let width_2 = 1f64;
        let pos_x = (x - 1) as f64 * 4f64 * scale + 16f64;
        obj_array.push(Box::new(Rectangle::new(AABB::new(Vec3::new(pos_x - width_2, 0f64, -1e3f64), Vec3::new(pos_x + width_2, 1e-2, 1e3)), 
        Material::new(Box::new(MonotoneTexture::new(Color::new(1f64, 1f64, 1f64))), Diffuse, Vec3::zero()))));
    }

    // 空
    obj_array.push(Box::new(Sphere::new(Vec3::zero(), 1e3f64, Material::new(Box::new(MonotoneTexture::new(Color::new(0.2f64, 0.4f64, 0.88f64))), Diffuse, Vec3::zero()))));

    // 太陽(メイン)
    let light_power = 8f64;
    obj_array.push(Box::new(Sphere::new(Vec3::new(0f64, 800f64, 0f64), 200f64, Material::new(Box::new(MonotoneTexture::new(Color::zero())), Diffuse, Vec3::new(light_power, light_power, light_power)))));

    // 道路
    obj_array.push(Box::new(Rectangle::new(AABB::new(Vec3::new(-1e3f64, 0f64, -1e3f64), Vec3::new(1e3f64, 0f64, 1e3f64)), Material::new(Box::new(MonotoneTexture::new(Color::new(0.2f64, 0.2f64, 0.2f64))), Diffuse, Vec3::zero()))));

    let obj = Box::new(ModelObject::new("../../data/sports_car/sportsCar.obj", scale, Vec3::zero()));

    let diff_scale = 6f64;

    let mut rand = Random::new();
    for x in 0..4 {
        for z in 0..20 {
            if x == 0 && z == 0 {
                continue;
            }
            let pos = Vec3::new(x as f64 * 4f64 * scale + rand.gen_range(-diff_scale, diff_scale), 0f64, z as f64 * 7f64 * scale + rand.gen_range(-diff_scale, diff_scale));
            let idx = rand::random::<u64>() % 6 + 1;
            let obj_path = format!("../../data/sports_car/sportsCar{}.mtl", idx);
            obj_array.push(Box::new(obj.clone(scale, pos, obj_path.as_str())));
        }
    }
    obj_array.push(obj);

    config.sample_per_pixel = 32;
    config.grid_size = 24;
    config.screen_resolution_x = 1920;
    config.screen_resolution_y = 1080;
    config.camera_pos = Vec3::new(45f64, 25f64, -50f64);
    config.camera_dir = Vec3::new(0f64, -0.20f64, 1.0f64).normalize();
    config.camera_up = Vec3::new(0f64, 1f64, 0f64);
    config.screen_width = 30f64 * (config.screen_resolution_x as f64 / config.screen_resolution_y as f64);
    config.screen_height = 30f64;
    config.camera_dist = 40f64;
    config.lense_radius = 1.2f64;
    config.lense_focal = 30f64;
    config.output_path = "result.png".to_string();

    // let camera = Box::new(PinHoleCamera::new(&config));
    let camera = Box::new(ThinLenseCamera::new(&config));

    let mut env = Environment::new(camera, config, time);
    env.push(obj_array);
    env
}

fn main() {

    let env = get_environment();

    render_parallel_prod(env);
    // render_parallel(env);
    // render(env);
}
