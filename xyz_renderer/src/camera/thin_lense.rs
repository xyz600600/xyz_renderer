use ::ray::Ray;
use ::random::Random;
use ::math::transform::Matrix4x4;
use ::config::Config;
use ::math::vec3::Vec3;
use ::math::vec2::Vec2;
use ::camera::Camera;
use std::f64::consts::PI;

struct Lense {
    radius: f64,
    focal: f64,
}

impl Lense {

    fn new(r: f64, f: f64) -> Lense {
        Lense {
            radius: r,
            focal: f
        }
    }

    fn sample(&self, rand: &mut Random) -> Vec2 {
        let r = (rand.gen_range(0f64, self.radius.powi(2)));
        let theta = rand.gen_range(-PI, PI);
        Vec2::new(r * theta.cos(), r * theta.sin())
    }
}

pub struct ThinLenseCamera {
    camera_to_world: Matrix4x4,
    ex_scale: f64,
    ey_scale: f64,
    screen_rightdown: Vec3,
    camera_pos: Vec3,
    lense: Lense,
}

impl ThinLenseCamera {
    pub fn new(config: &Config) -> ThinLenseCamera {
        let _ey_scale = config.screen_height / (config.screen_resolution_y as f64);
        let _ex_scale = -config.screen_width / (config.screen_resolution_x as f64);
        let _screen_rightdown = Vec3::new(
            config.screen_width / 2f64, 
            -config.screen_height / 2f64, 
            -config.camera_dist
        );

        ThinLenseCamera {
            camera_to_world: Matrix4x4::look_at(&config.camera_pos, &config.camera_dir, &config.camera_up),
            ey_scale: _ey_scale,
            ex_scale: _ex_scale,
            camera_pos: config.camera_pos,
            screen_rightdown: _screen_rightdown,
            lense: Lense::new(config.lense_radius, config.lense_focal),
        }
    }
}

impl Camera for ThinLenseCamera {
    fn get_initial_ray(&self, y: usize, x: usize, rand: &mut Random) -> (f64, Ray) {
        let screen_pos = Vec3::new(
            self.screen_rightdown.x() + self.ex_scale * (x as f64 + rand.gen_range(0f64, 1f64)), 
            self.screen_rightdown.y() + self.ey_scale * (y as f64 + rand.gen_range(0f64, 1f64)), 
            self.screen_rightdown.z()
        );
        let lense_pos_c: Vec3 = self.lense.sample(rand).into();
        let lense_pos = self.camera_to_world.apply(lense_pos_c);
        let b = self.screen_rightdown.z().abs();
        let obj_plane_t = b * self.lense.focal / (b - self.lense.focal);

        let obj_plane_pos = self.camera_to_world.apply(-screen_pos * (obj_plane_t / b));
        let dir = (obj_plane_pos - lense_pos).normalize();
        
        // (0f64, 0f64, 1f64) との dot 積を取れば theta が出る
        let cos_init = (lense_pos_c - screen_pos).normalize().z();

        (8000f64 * cos_init.powi(4) / b.powi(2), Ray::new(lense_pos, dir, 1f64))
    }

    fn camera_dir(&self) -> Vec3 {
        let mat = &self.camera_to_world;
        Vec3::new(mat[0][2], mat[1][2], mat[2][2])
    }
}