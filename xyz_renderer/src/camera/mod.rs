use ::ray::Ray;
use ::random::Random;
use ::math::vec3::Vec3;

pub trait Camera: Sync + Send {
    fn get_initial_ray(&self, y: usize, x: usize, rand: &mut Random) -> (f64, Ray);

    fn camera_dir(&self) -> Vec3;
}

pub mod pinhole;
pub mod thin_lense;