use ::ray::Ray;
use ::random::Random;
use ::math::transform::Matrix4x4;
use ::config::Config;
use ::math::vec3::Vec3;
use ::camera::Camera;


#[derive(Debug)]
pub struct PinHoleCamera {
    camera_to_world: Matrix4x4,
    camera_pos: Vec3,
    screen_rightdown: Vec3,
    ex_scale: f64,
    ey_scale: f64,
}

impl PinHoleCamera {
    pub fn new(config: &Config) -> PinHoleCamera {
        let _ey_scale = config.screen_height / (config.screen_resolution_y as f64);
        let _ex_scale = -config.screen_width / (config.screen_resolution_x as f64);
        let _screen_rightdown = Vec3::new(
            config.screen_width / 2f64, 
            -config.screen_height / 2f64, 
            -config.camera_dist
        );

        PinHoleCamera {
            camera_to_world: Matrix4x4::look_at(&config.camera_pos, &config.camera_dir, &config.camera_up),
            camera_pos: config.camera_pos,
            screen_rightdown: _screen_rightdown,
            ey_scale: _ey_scale,
            ex_scale: _ex_scale,
        }
    }
}

impl Camera for PinHoleCamera {
    fn get_initial_ray(&self, y: usize, x: usize, rand: &mut Random) -> (f64, Ray) {
        let target = Vec3::new(
            self.screen_rightdown.x() + self.ex_scale * (x as f64 + 0.5f64), 
            self.screen_rightdown.y() + self.ey_scale * (y as f64 + 0.5f64), 
            self.screen_rightdown.z()
        );
        let real_dir = (self.camera_pos - self.camera_to_world.apply(target)).normalize();
        (1f64, Ray::new(self.camera_pos, real_dir, 1f64))
    }

    fn camera_dir(&self) -> Vec3 {
        let mat = &self.camera_to_world;
        Vec3::new(mat[0][2], mat[1][2], mat[2][2])
    }
}

// カメラの中心を(2, 3, 4), x-z の比例方向対角線に向かってレイを飛ばす
// screenをx-zの反比例方向に置く
// 初期Rayが、対角線上に厳密に載るように調整して、対角線上のRayが取れるかを確認
#[test]
fn test_get_initial_ray() {
    let mut config = Config::new();

    config.screen_height = 2f64;
    config.screen_width = 2f64.sqrt();
    config.screen_resolution_y = 401usize;
    config.screen_resolution_x = 401usize;
    config.camera_up = Vec3::new(0f64, 1f64, 0f64);
    config.camera_dir = Vec3::new(1f64, 0f64, 1f64).normalize();
    config.camera_pos = Vec3::new(2f64, 3f64, 4f64);
    config.camera_dist = 1f64 / 2f64.sqrt();

    let camera = PinHoleCamera::new(&config);
    let mut rand = Random::new();

    let (pdf, init_ray) = camera.get_initial_ray(200usize, 200usize, &mut rand);

    println!("{}, {}, {}", &init_ray.dir.x(), &init_ray.dir.y(), &init_ray.dir.z());
    println!("{}, {}, {}", &init_ray.pos.x(), &init_ray.pos.y(), &init_ray.pos.z());

    assert_eq!(init_ray.dir.x(), 1f64 / 2f64.sqrt());
    assert_eq!(init_ray.dir.y(), 0f64);
    assert_eq!(init_ray.dir.z(), 1f64 / 2f64.sqrt());

    assert_eq!(init_ray.pos.x(), 2f64);
    assert_eq!(init_ray.pos.y(), 3f64);
    assert_eq!(init_ray.pos.z(), 4f64);
}