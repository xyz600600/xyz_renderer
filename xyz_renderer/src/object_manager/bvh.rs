use ::object::{Object, Intersectable, Primitive};
use ::object::model_object::ModelFace;
use ::object::rectangle::AABB;
use ::math::vec3::Vec3;
use ::ray::Ray;
use ::common::INF;
use ::random::Random;
use ::util::fast_stack::FastStack;

use ::std::collections::{VecDeque};
use ::std::cmp::{Ordering};
use std::mem;

#[derive(Debug)]
pub struct Node {
    pub is_leaf: bool,
    pub v1: usize,
    pub v2: usize,
    pub bounding_box: AABB,
}

impl Node {
    pub fn new_inner(_is_leaf: bool, _left_idx: usize, _right_idx: usize, _bb: AABB) -> Node {
        Node {
            is_leaf: _is_leaf,
            v1: _left_idx,
            v2: _right_idx,
            bounding_box: _bb,
        }
    }
    pub fn new_leaf(_is_leaf: bool, _leaf_from: usize, _leaf_to: usize, _bb: AABB) -> Node {
        Node {
            is_leaf: _is_leaf,
            v1: _leaf_from,
            v2: _leaf_to,
            bounding_box: _bb,
        }
    }

    pub fn left_idx(&self) -> usize {
        assert!(!self.is_leaf);
        self.v1
    }
    pub fn right_idx(&self) -> usize {
        assert!(!self.is_leaf);
        self.v2
    }
    pub fn leaf_from(&self) -> usize {
        assert!(self.is_leaf);
        self.v1
    }
    pub fn leaf_to(&self) -> usize {
        assert!(self.is_leaf);
        self.v2
    }
    pub fn left_idx_mut(&mut self) -> &mut usize {
        assert!(!self.is_leaf);
        &mut self.v1
    }
    pub fn right_idx_mut(&mut self) -> &mut usize {
        assert!(!self.is_leaf);
        &mut self.v2
    }
    pub fn leaf_from_mut(&mut self) -> &mut usize {
        assert!(self.is_leaf);
        &mut self.v1
    }
    pub fn leaf_to_mut(&mut self) -> &mut usize {
        assert!(self.is_leaf);
        &mut self.v2
    }
}

pub struct BVH2ModelFace {
    pub obj_array: Vec<ModelFace>,
    pub node_array: Vec<Node>,
}

impl BVH2ModelFace {

    fn reorder_by(index_of: &mut Vec<usize>, left_aabb_ary: &mut Vec<AABB>, right_aabb_ary: &mut Vec<AABB>, aabb_ary_single: &Vec<AABB>,
                  from: usize, to: usize, axis: usize) {
        index_of[from..to].sort_by(|idx1, idx2| {
            let v1 = aabb_ary_single[*idx1].max[axis];
            let v2 = aabb_ary_single[*idx2].max[axis];
            if v1 < v2 {
                Ordering::Less
            } else if v1 > v2 {
                Ordering::Greater
            } else {
                idx1.cmp(idx2)
            }
        });
        // 2. SAH を最も小さくする左右の切り方を求める
        left_aabb_ary[from] = aabb_ary_single[index_of[from]];
        for i in from..to-1 {
            left_aabb_ary[i + 1] = aabb_ary_single[index_of[i + 1]];
            let prev = left_aabb_ary[i];
            left_aabb_ary[i + 1].union(&prev);
        }
        right_aabb_ary[to - 1] = aabb_ary_single[index_of[to - 1]];
        for i in (from+1..to).rev() {
            right_aabb_ary[i - 1] = aabb_ary_single[index_of[i - 1]];
            let prev = right_aabb_ary[i];
            right_aabb_ary[i - 1].union(&prev);
        }
    }

    // ret: (axis, split_idx)
    pub fn select_best_split(index_of: &mut Vec<usize>, left_aabb_ary: &mut Vec<AABB>, right_aabb_ary: &mut Vec<AABB>, aabb_ary_single: &Vec<AABB>,
                         from: usize, to: usize, step: usize) -> (usize, usize) {
        let mut best_axis = 0usize;
        let mut best_split_idx = from + 1;
        let mut best_sah = INF;
        for axis in 0..3 {
            BVH2ModelFace::reorder_by(index_of, left_aabb_ary, right_aabb_ary, aabb_ary_single, from, to, axis);
            // TODO: use iterator
            let calc_sah = |i: &usize| {
                left_aabb_ary[*i - 1].area() * (*i - from) as f64 + 
                right_aabb_ary[*i].area() * (to - *i) as f64
            };
            // this index is exclusive. i.e. [from, split_idx), [split_idx, to)
            for i in (from+step..to).step_by(step) {
                let sah = calc_sah(&i);
                if best_sah > sah {
                    best_sah = sah;
                    best_split_idx = i;
                    best_axis = axis;
                }
            }
        }
        if best_axis != 2 {
            BVH2ModelFace::reorder_by(index_of, left_aabb_ary, right_aabb_ary, aabb_ary_single, from, to, best_axis);
        }
        (best_axis, best_split_idx)
    }

    pub fn construct_node_array(node_array: &mut Vec<Node>, index_of: &mut Vec<usize>, left_aabb_ary: &mut Vec<AABB>, right_aabb_ary: &mut Vec<AABB>, aabb_ary_single: &Vec<AABB>) {

        // node_idx, depth,
        let mut que: VecDeque<(usize, usize, usize, usize)> = VecDeque::new();
        que.push_back((0, 0, 0, index_of.len()));

        while let Some((parent_idx, depth, from, to)) = que.pop_front() {
            
            let (best_axis, best_split_idx) = BVH2ModelFace::select_best_split(index_of, left_aabb_ary, right_aabb_ary, aabb_ary_single, from, to, 1);

            // bounding_box 計算用
            if depth == 0 {
                node_array[parent_idx].bounding_box = right_aabb_ary[from];
            }

            // 3. オブジェクトの数がT個を下回っていたら、葉ノードになる。そうでなければ、区間を切ってやり直し

            // left
            // 1つnodeを追加した後の最後尾をさすので、len() - 1 ではない
            let left_idx = node_array.len();
            if best_split_idx - from <= 8 {
                node_array.push(Node::new_leaf(true, from, best_split_idx, left_aabb_ary[best_split_idx - 1]));
            } else {
                node_array.push(Node::new_inner(false, 0, 0, left_aabb_ary[best_split_idx - 1]));
                que.push_back((left_idx, depth + 1, from, best_split_idx));
            }
            *node_array[parent_idx].left_idx_mut() = left_idx;

            // right
            let right_idx = node_array.len();
            if to - best_split_idx <= 8 {
                node_array.push(Node::new_leaf(true, best_split_idx, to, right_aabb_ary[best_split_idx]));
            } else {
                node_array.push(Node::new_inner(false, 0, 0, right_aabb_ary[best_split_idx]));
                que.push_back((right_idx, depth + 1, best_split_idx, to));
            }
            *node_array[parent_idx].right_idx_mut() = right_idx;
        }
    }

    pub fn reorder_object<T: Sized>(values: &mut Vec<usize>, obj_array: &mut Vec<T>) {

        let mut index_of = vec![0usize; obj_array.len()];
        for i in 0..obj_array.len() {
            index_of[values[i]] = i;
        }

        for v in 0..obj_array.len() {
            obj_array.swap(v, values[v]);
            let prev_index_i = values[v];
            values.swap(v, index_of[v]);
            index_of.swap(v, prev_index_i);
        }
    }

    pub fn construct(&mut self, obj_array: Vec<ModelFace>) {
        let len = obj_array.len();

        self.obj_array = obj_array;

        let mut left_aabb_ary = vec![AABB::new(Vec3::zero(), Vec3::zero()); len];
        let mut right_aabb_ary = vec![AABB::new(Vec3::zero(), Vec3::zero()); len];
        self.node_array.push(Node::new_inner(false, 0, 0, AABB::new(Vec3::zero(), Vec3::zero())));

        let mut index_of = (0..len).collect::<Vec<usize>>();
        
        let aabb_ary_single = (0..len).map(|idx|{
            self.obj_array[idx].bounding_box()
        }).collect::<Vec<AABB>>();

        BVH2ModelFace::construct_node_array(&mut self.node_array, &mut index_of, &mut left_aabb_ary, &mut right_aabb_ary, &aabb_ary_single);

        BVH2ModelFace::reorder_object(&mut index_of, &mut self.obj_array);
    }

    pub fn new() -> BVH2ModelFace {
        BVH2ModelFace {
            obj_array: vec![],
            node_array: vec![],
        }
    }
    
    pub fn len(&self) -> usize {
        self.obj_array.len()
    }

    pub fn bounding_box(&self) -> AABB {
        self.node_array[0].bounding_box
    }

    pub fn intersect(&self, ray: &Ray) -> Option<(&Primitive, f64)> {
        let mut min_index = 0;
        let mut min_t = INF;
        let mut stack = FastStack::new();
        stack.push((0f64, 0));

        while let Some((dist, node_idx)) = stack.pop() {
            let node = &self.node_array[node_idx];
            
            if dist < min_t {
                if node.is_leaf {
                    for idx in node.leaf_from()..node.leaf_to() {
                        let t = self.obj_array[idx].intersect(&ray);

                        if min_t > t {
                            min_t = t;
                            min_index = idx;
                        }
                    }
                } else {
                    let mut item_array = [
                        (self.node_array[node.left_idx()].bounding_box.intersect(&ray), node.left_idx()), 
                        (self.node_array[node.right_idx()].bounding_box.intersect(&ray), node.right_idx()),
                    ];
                    if item_array[0].0 < item_array[1].0 {
                        item_array.swap(0, 1);
                    }
                    if item_array[0].0 < min_t {
                        stack.push(item_array[0]);
                    }
                    if item_array[1].0 < min_t {
                        stack.push(item_array[1]);
                    }
                }
            }
        }
        if min_t == INF {
            None
        } else {
            Some((&self.obj_array[min_index], min_t))
        }
    }
}

pub struct BVH2Object {
    pub obj_array: Vec<Box<Object>>,
    pub node_array: Vec<Node>,
}

impl BVH2Object {

    pub fn new() -> BVH2Object {
        BVH2Object {
            obj_array: vec![],
            node_array: vec![],
        }
    }

    pub fn len(&self) -> usize {
        self.obj_array.len()
    }

    pub fn construct(&mut self, obj_array: Vec<Box<Object>>) {
        let len = obj_array.len();
        self.obj_array = obj_array;

        let mut left_aabb_ary = vec![AABB::new(Vec3::zero(), Vec3::zero()); len];
        let mut right_aabb_ary = vec![AABB::new(Vec3::zero(), Vec3::zero()); len];
        self.node_array.push(Node::new_inner(false, 0, 0, AABB::new(Vec3::zero(), Vec3::zero())));

        let mut index_of = (0..len).collect::<Vec<usize>>();
        
        let aabb_ary_single = (0..len).map(|idx|{
            self.obj_array[idx].bounding_box()
        }).collect::<Vec<AABB>>();

        BVH2ModelFace::construct_node_array(&mut self.node_array, &mut index_of, &mut left_aabb_ary, &mut right_aabb_ary, &aabb_ary_single);

        BVH2ModelFace::reorder_object(&mut index_of, &mut self.obj_array);
    }
}

impl Intersectable for BVH2Object {

    fn bounding_box(&self) -> AABB {
        self.node_array[0].bounding_box
    }

    fn intersect(&self, ray: &Ray) -> Option<(&Object, &Primitive, f64)> {
        let mut stack = FastStack::new();
        stack.push(0);
        let mut result: Option<(&Object, &Primitive, f64)> = None;

        while let Some(node_idx) = stack.pop() {
            let node = &self.node_array[node_idx];

            if node.is_leaf {
                for idx in node.leaf_from()..node.leaf_to() {
                    let t = self.obj_array[idx].intersect(&ray);

                    if result.is_none() || (t.is_some() && result.unwrap().2 > t.unwrap().2) {
                        result = t;
                    }
                }
            } else {
                let mut idx_ary = [
                    node.left_idx(),
                    node.right_idx()
                ];
                let mut t_ary = [
                    self.node_array[idx_ary[0]].bounding_box.intersect(&ray),
                    self.node_array[idx_ary[1]].bounding_box.intersect(&ray),
                ];
                
                let first = if t_ary[0] < t_ary[1] {
                    1
                } else {
                    0
                };
                let second = 1 - first;

                if result.is_none() || t_ary[first] < result.unwrap().2 {
                    stack.push(idx_ary[first]);
                }
                if result.is_none() || t_ary[second] < result.unwrap().2 {
                    stack.push(idx_ary[second]);
                }
            }
        }
        result
    }
}