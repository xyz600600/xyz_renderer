use object_manager::bvh::{Node, BVH2ModelFace, BVH2Object};
use object::rectangle::{AABB, AABBx4};
use object::model_object::ModelFace;
use object::{Intersectable, Object, Primitive};
use ray::Ray;
use util::fast_stack::FastStack;
use common::{EPS, INF};
use math::vec3::{Vec3, Vec3x4};

use ::std::collections::{VecDeque};
use ::std::cmp::{Ordering};

use std::collections::HashSet;

// AABB を親が持つ必要があるので、全部親が持つことにする
// Leaf Node は存在しない
pub struct Nodex4 {
    pub is_leaf: bool,
    pub vs: [usize; 4],
    pub bounding_box: AABBx4,
}

impl Nodex4 {
    pub fn new_inner() -> Nodex4 {
        Nodex4 {
            is_leaf: false,
            vs: [0usize; 4],
            bounding_box: AABBx4::new(AABB::init_for_union(), AABB::init_for_union(), AABB::init_for_union(), AABB::init_for_union())
        }
    }

    pub fn new_leaf(from: usize, to: usize) -> Nodex4 {
        Nodex4 {
            is_leaf: true,
            vs: [from, to, 0, 0],
            bounding_box: AABBx4::new(AABB::init_for_union(), AABB::init_for_union(), AABB::init_for_union(), AABB::init_for_union())
        }
    }

    pub fn child_idx(&self, idx: usize) -> usize {
        self.vs[idx]
    }

    pub fn child_idx_mut(&mut self, idx: usize) -> &mut usize {
        &mut self.vs[idx]
    }

    pub fn leaf_from(&self) -> usize {
        self.vs[0]
    }

    pub fn leaf_from_mut(&mut self) -> &mut usize {
        &mut self.vs[0]
    }

    pub fn leaf_to(&self) -> usize {
        self.vs[1]
    }

    pub fn leaf_to_mut(&mut self) -> &mut usize {
        &mut self.vs[1]
    }
}

pub struct BVH4ModelFace {
    pub obj_array: Vec<ModelFace>,
    pub node_array: Vec<Nodex4>,
    _bounding_box: AABB,
}

// BVH2 を展開することで作る
impl BVH4ModelFace {

    pub fn new() -> BVH4ModelFace {
        BVH4ModelFace {
            obj_array: vec![],
            node_array: vec![],
            _bounding_box: AABB::init_for_union(),
        }
    }

    pub fn construct(&mut self, obj_array: Vec<ModelFace>) {
        self.node_array.push(Nodex4::new_inner());

        self.obj_array = obj_array;
        let len = self.obj_array.len();

        let mut values = (0..len).collect::<Vec<usize>>();

        let mut left_aabb_ary = vec![AABB::new(Vec3::pos_inf(), Vec3::neg_inf()); len];
        let mut right_aabb_ary = vec![AABB::new(Vec3::pos_inf(), Vec3::neg_inf()); len];

        let aabb_ary_single = (0..len).map(|idx|{
            self.obj_array[idx].bounding_box()
        }).collect::<Vec<AABB>>();

        let mut que: VecDeque<(usize, usize, usize)> = VecDeque::new();
        que.push_back((0, 0, values.len()));

        let leaf_size = 8;

        while let Some((parent_idx, from, to)) = que.pop_front() {
            let (best_axis, best_split_idx) = BVH2ModelFace::select_best_split(&mut values, &mut left_aabb_ary, &mut right_aabb_ary, &aabb_ary_single, from, to, leaf_size / 2);
            
            if from == 0 && to == len {
                self._bounding_box = right_aabb_ary[0];
            }

            let (best_axis_l, best_split_idx_l) = BVH2ModelFace::select_best_split(&mut values, &mut left_aabb_ary, &mut right_aabb_ary, &aabb_ary_single, from, best_split_idx, leaf_size / 4);
            let (best_axis_r, best_split_idx_r) = BVH2ModelFace::select_best_split(&mut values, &mut left_aabb_ary, &mut right_aabb_ary, &aabb_ary_single, best_split_idx, to, leaf_size / 4);
            
            let splits = [from, best_split_idx_l, best_split_idx, best_split_idx_r, to];

            self.node_array[parent_idx].bounding_box = AABBx4::new(
                left_aabb_ary[best_split_idx_l], right_aabb_ary[best_split_idx_l], left_aabb_ary[best_split_idx_r], right_aabb_ary[best_split_idx_r]
            );
            for idx in 0..4 {
                let child_idx = self.node_array.len();
                *self.node_array[parent_idx].child_idx_mut(idx) = child_idx;
                if splits[idx + 1] - splits[idx] >= leaf_size {
                    self.node_array.push(Nodex4::new_inner());
                    que.push_back((child_idx, splits[idx], splits[idx + 1]));
                } else {
                    self.node_array.push(Nodex4::new_leaf(splits[idx], splits[idx + 1]));
                }
            }            
        }
        BVH2ModelFace::reorder_object(&mut values, &mut self.obj_array);
    }

    pub fn bounding_box(&self) -> AABB {
        self._bounding_box
    }

    pub fn intersect(&self, ray: &Ray) -> Option<(&Primitive, f64)> {
        let mut min_index = 0;
        let mut min_t = INF;
        let mut stack = vec![];
        stack.push((0f64, 0));

        while let Some((dist, node_idx)) = stack.pop() {
            let node = &self.node_array[node_idx];
            if dist < min_t {
                if node.is_leaf {
                    for idx in node.leaf_from()..node.leaf_to() {
                        let t = self.obj_array[idx].intersect(&ray);
                        if min_t > t {
                            min_t = t;
                            min_index = idx;
                        }
                    }
                } else {
                    let mut vs = [(0f64, 0); 4];
                    let dists = node.bounding_box.intersect(&ray);
                    for i in 0..4 {
                        vs[i] = (dists[i], node.child_idx(i));
                    }
                    vs.sort_by(|v1, v2| {
                        if v1.0 < v2.0 {
                            Ordering::Less
                        } else if v1.0 > v2.0 {
                            Ordering::Greater
                        } else {
                            v1.1.cmp(&v2.1)
                        }
                    });
                    for i in (0..4).rev() {
                        if vs[i].0 < min_t {
                            stack.push(vs[i]);
                        }
                    }
                }
            }
        }
        if min_t == INF {
            None
        } else {
            Some((&self.obj_array[min_index], min_t))
        }
    }
}