use std::vec::Vec;
use std::ops::{Index, IndexMut};

use ::common::INF;
use ::object::rectangle::AABB;
use ::object::{Intersectable, Object, Material, Primitive};
use ::ray::Ray;
use ::math::vec3::Vec3;
use ::random::Random;

use ::std::cmp::Ordering;

pub struct LinearObjectManager {
    pub obj_array: Vec<Box<Object>>,
    pub light_list: Vec<usize>,
    _bounding_box: AABB,
}

impl LinearObjectManager {

    pub fn new() -> LinearObjectManager {
        LinearObjectManager {
            obj_array: vec![],
            light_list: vec![],
            _bounding_box: AABB::new(Vec3::pos_inf(), Vec3::neg_inf()),
        }
    }

    pub fn push(&mut self, obj: Box<Object>) {
        self._bounding_box.union(&obj.bounding_box());
        self.obj_array.push(obj);
    }

    pub fn construct(&mut self, obj_list: Vec<Box<Object>>) {
        for obj in obj_list {
            self._bounding_box.union(&obj.bounding_box());
            self.obj_array.push(obj);
        }
    }
}

impl Intersectable for LinearObjectManager {

    fn intersect(&self, ray: &Ray) -> Option<(&Object, &Primitive, f64)> {
        self.obj_array.iter()
            .map(|ref x| x.intersect(&ray))
            .filter_map(|v| v)
            .min_by(|v1, v2| {
                if v1.2 < v2.2 {
                    Ordering::Less
                } else {
                    Ordering::Greater
                }
            })
    }

    fn bounding_box(&self) -> AABB {
        self._bounding_box
    }
}

#[test]
fn test_sphere_intersect() {

    use ::texture::monotone_texture::MonotoneTexture;
    use ::math::vec3::Color;
    use ::object::sphere::Sphere;
    use ::brdf::ReflectionType;
    use ::math::vec3::Vec3;
    use ::common::EPS;

    let mut manager = LinearObjectManager::new();

    for i in 0..10 {
        let c = (10 - i) as f64;
        let sphere = Box::new(Sphere::new(
            Vec3::new(c, c, c), 
            1f64, 
            Material::new(
                Box::new(MonotoneTexture::new(Color::new(c, 0.5f64, 0.5f64))),
                ReflectionType::Diffuse,
                Vec3::new(0f64, 0f64, 0f64)
            )
        ));
        manager.push(sphere);
    }
    
    let ray = Ray::new(Vec3::new(0f64, 0f64, 0f64), Vec3::new(1f64, 1f64, 1f64).normalize(), 1f64);
    let (obj, prim, dist) = manager.intersect(&ray).unwrap();

    let expected_dist = 3f64.sqrt() - 1f64;
    assert!((dist - expected_dist).abs() < EPS);
    assert_eq!(obj.material(prim.material_id()).texture.color().x(), 1f64);
}