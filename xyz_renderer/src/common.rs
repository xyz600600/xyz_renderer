use ::std::cmp::{Ord, Ordering};

pub fn clamp(min_val: f64, val: f64, max_val: f64) -> f64 {
    val.max(min_val).min(max_val)
}

pub const EPS: f64 = 1e-5;
pub const INF: f64 = 1e20;

#[test]
fn test_clamp() {
    assert_eq!(clamp(0f64, 0.5f64, 1f64), 0.5f64);
    assert_eq!(clamp(0f64, -1f64, 1f64), 0f64);
    assert_eq!(clamp(0f64, 2f64, 1f64), 1f64);    
}