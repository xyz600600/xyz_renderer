use ::math::vec3::Vec3;

pub struct Config {
    pub screen_height: f64,
    pub screen_width: f64,
    pub screen_resolution_y: usize,
    pub screen_resolution_x: usize,
    pub grid_size: usize,
    pub camera_up: Vec3,
    pub camera_dir: Vec3,
    pub camera_pos: Vec3,
    pub camera_dist: f64,
    pub sample_per_pixel: usize,
    pub output_path: String,
    pub lense_radius: f64,
    pub lense_focal: f64,
    pub light_index: usize,
}

impl Config {
    pub fn new() -> Config {
        Config {
            screen_height: 0f64,
            screen_width: 0f64,
            screen_resolution_y: 0usize,
            screen_resolution_x: 0usize,
            grid_size: 0usize,
            camera_up: Vec3::new(0f64, 0f64, 0f64),
            camera_dir: Vec3::new(0f64, 0f64, 0f64),
            camera_pos: Vec3::new(0f64, 0f64, 0f64),
            camera_dist: 0f64,
            sample_per_pixel: 0usize,
            output_path: "".to_string(),
            lense_radius: 0f64,
            lense_focal: 0f64,
            light_index: 0,
        }
    }
}