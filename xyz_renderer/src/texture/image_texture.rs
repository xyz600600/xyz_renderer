use ::math::vec3::Color;
use ::math::vec2::Vec2;
use ::texture::Texture;
use ::image::Image;

pub struct ImageTexture {
    image: Image,
}

impl ImageTexture {
    pub fn new(img: Image) -> ImageTexture {
        ImageTexture {
            image: img
        }
    }
}

impl Texture for ImageTexture {
    fn color(&self) -> Color {
        self.image[0][0]
    }

    fn color_of(&self, vt: &Vec2) -> Color {
        let y = (vt.y().fract() * (self.image.height as f64 - 1f64)).round() as usize;
        let x = (vt.x().fract() * (self.image.width as f64 - 1f64)).round() as usize;
        self.image[y][x]
    }
}