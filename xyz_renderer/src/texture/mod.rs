use ::math::vec3::Color;
use ::math::vec2::Vec2;

pub trait Texture : Sync + Send {
    fn color(&self) -> Color;
    fn color_of(&self, vt: &Vec2) -> Color;
}

pub mod monotone_texture;
pub mod image_texture;