use ::math::vec3::Color;
use ::math::vec2::Vec2;
use ::texture::Texture;

pub struct MonotoneTexture {
    color: Color
}

impl MonotoneTexture {
    pub fn new(_color: Color) -> MonotoneTexture {
        MonotoneTexture {
            color: _color,
        }
    }
}

impl Texture for MonotoneTexture {
    fn color(&self) -> Color {
        self.color
    }

    fn color_of(&self, vt: &Vec2) -> Color {
        self.color
    }
}