
extern crate rand;

use random::rand::{Rng, SeedableRng, IsaacRng};

#[derive(Clone)]
pub struct Random {
    rand: IsaacRng,
}

impl Random {

    pub fn from_seed(s: usize) -> Random {
	    let mut seeds = [0u32; 8];
        for j in 0..8 {
            seeds[j] = (s + j) as u32;
        }
        Random {
            rand: rand::IsaacRng::from_seed(&seeds),
        }
    }

    pub fn new() -> Random {
        Random {
            rand: rand::IsaacRng::new_unseeded(),
        }
    }

    pub fn gen_range(&mut self, from: f64, to: f64) -> f64 {
        self.rand.gen_range(from, to)
    }
}
