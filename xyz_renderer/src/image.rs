use math::vec3::Color;

use std::ops::{Index, IndexMut};
use std::path::Path;

extern crate image;

pub struct Image {
    pub height: usize,
    pub width: usize,
    data: Vec<Vec<Color>>,
}

impl Image {
    pub fn new(_height: usize, _width: usize) -> Image {
        let mut img = Image {
            height: _height,
            width: _width,
            data: vec![],
        };
        img.data.resize(img.height, vec![]);

        for ref mut row in &mut img.data {
            row.resize(img.width, Color::new(0f64, 0f64, 0f64));
        }
        img
    }

    pub fn load(_filepath: &Path) -> Image {
        Image::convert_from(image::open(_filepath).ok().expect("Opening image failed").to_rgb())
    }

    pub fn save(&self, _filepath: &Path) {
        self.convert_to().save(_filepath).ok().expect("Saving image failed");
    }

    // RgbImage -> Image
    fn convert_from(src: image::RgbImage) -> Image {
        let mut ret = Image::new(src.height() as usize, src.width() as usize);

        let convert = |val: u8| -> f64 {
            ((val as f64) / 255f64).powf(2.2f64)
        };

        for row in 0..ret.height {
            for col in 0..ret.width {
                let ref mut pix = src.get_pixel(col as u32, row as u32);
                ret[row][col] = Color::new(convert(pix[0]), convert(pix[1]), convert(pix[2]))
            }
        }
        ret
    }

    // Image -> RgbImage
    fn convert_to(&self) -> image::RgbImage {

        use ::common::clamp;

        let mut ret = image::RgbImage::new(self.width as u32, self.height as u32);

        let convert = |val: f64| -> u8 {
            f64::round(clamp(0f64, val, 1f64).powf(1f64 / 2.2f64) * (u8::max_value() as f64)) as u8
        };

        for row in 0..self.height {
            for col in 0..self.width {
                let ref mut pix = ret.get_pixel_mut(col as u32, row as u32);
                pix[0] = convert(self[row][col].x());
                pix[1] = convert(self[row][col].y());
                pix[2] = convert(self[row][col].z());
            }
        }
        ret
    }
}

impl Index<usize> for Image {
    type Output = Vec<Color>;

    fn index(&self, index: usize) -> &Vec<Color> {
        &self.data[index]
    }
}

impl IndexMut<usize> for Image {
    fn index_mut(&mut self, index: usize) -> &mut Vec<Color> {
        &mut self.data[index]
    }
}

pub struct SubImage {
    pub height: usize,
    pub width: usize,
    pub sx: usize,
    pub sy: usize,
    data: Vec<Vec<Color>>,        
}

impl SubImage {
    pub fn new(_height: usize, _width: usize, _sx: usize, _sy: usize) -> SubImage {
        SubImage {
            height: _height,
            width: _width,
            sy: _sy,
            sx: _sx,
            data: vec![vec![Color::zero(); _width]; _height],
        }
    }

    pub fn create_grid_subimage_list(width: usize, height: usize, grid_size: usize) -> Vec<SubImage> {
        assert!(width % grid_size == 0);
        assert!(height % grid_size == 0);

        let mut ret: Vec<SubImage> = vec![];

        for y in (0..height).step_by(grid_size) {
            for x in (0..width).step_by(grid_size) {
                ret.push(SubImage::new(grid_size, grid_size, y, x));
            }
        }
        ret
    }

    pub fn update_to(&self, img: &mut Image, spp: usize) {
        for y in self.sy..(self.sy+self.height) {
            for x in self.sx..(self.sx+self.width) {
                img[y][x] = self.data[y - self.sy][x - self.sx] / (spp as f64);
            }
        }
    }
}

impl Index<usize> for SubImage {
    type Output = Vec<Color>;

    fn index(&self, index: usize) -> &Vec<Color> {
        &self.data[index]
    }
}

impl IndexMut<usize> for SubImage {
    fn index_mut(&mut self, index: usize) -> &mut Vec<Color> {
        &mut self.data[index]
    }
}

#[test]
fn test_image_index() {
    let height = 200;
    let width = 400;
    let mut img = Image::new(height, width);
    println!("len = {}", img.data.len());
    for row in 0..height {
        for col in 0..width {
            img[row][col] = Color::new(
                (row + col) as f64,
                (row * col) as f64,
                (row * col) as f64
            );
        }
    }

    // set as immutable
    let img = img;

    for row in 0..height {
        for col in 0..width {
            assert_eq!(img[row][col].x(), (row + col) as f64);
            assert_eq!(img[row][col].y(), (row * col) as f64);
            assert_eq!(img[row][col].z(), (row * col) as f64);
        }
    }
}

#[test]
fn test_image_load_save() {
    let height = 400;
    let width = 200;
    let mut img = Image::new(height, width);

    for row in 0..height {
        for col in 0..width {
            img[row][col] = Color::new(
                ((row + col) as f64) / ((height + width) as f64),
                ((row * col) as f64) / ((height * width) as f64),
                (row.max(col) as f64) / (height.max(width) as f64),
            );
        }
    }

    let path = Path::new("result.png");
    img.save(path);
    let img2 = Image::load(path);

    for row in 0..height {
        for col in 0..width {
            assert!((img[row][col].x() - img2[row][col].x()).abs() < 2f64 / 255f64);
            assert!((img[row][col].y() - img2[row][col].y()).abs() < 2f64 / 255f64);
            assert!((img[row][col].z() - img2[row][col].z()).abs() < 2f64 / 255f64);
        }
    }
}
