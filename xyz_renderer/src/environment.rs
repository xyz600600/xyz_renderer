use object_manager::linear_object_manager::LinearObjectManager;
use object_manager::bvh::BVH2Object;
use camera::pinhole::PinHoleCamera;
use camera::Camera;
use ::config::Config;
use ::object::{Object, Primitive, Intersectable};
use ::object::sphere::Sphere;

use std::time::Instant;

pub struct Environment {
    pub camera: Box<Camera>,
    pub obj_manager: BVH2Object,
    pub light_list: Vec<usize>,

    pub resolution_y: usize,
    pub resolution_x: usize,
    pub grid_size: usize,
    pub sample_per_pixel: usize,
    pub output_path: String,
    pub time: Instant,
}

impl Environment {
    pub fn new(_camera: Box<Camera>, 
            config: Config,
            _time: Instant) -> Environment {
        Environment {
            camera: _camera,
            obj_manager: BVH2Object::new(),
            light_list: vec![],

            resolution_y: config.screen_resolution_y,
            resolution_x: config.screen_resolution_x,
            grid_size: config.grid_size,
            sample_per_pixel: config.sample_per_pixel,
            output_path: config.output_path,
            time: _time,
        }
    }

    pub fn push(&mut self, obj_array: Vec<Box<Object>>) {
        self.obj_manager.construct(obj_array);
        for (idx, item) in self.obj_manager.obj_array.iter().enumerate() {
            if let Some(prim) = item.light() {
                self.light_list.push(idx);
            }
        }
    }

    pub fn light(&self, idx: usize) -> &Object {
        &*self.obj_manager.obj_array[self.light_list[idx]]
    }
}