use std::ops::{Add, Sub, Mul, Div, AddAssign, SubAssign, MulAssign, DivAssign, Neg, Index, IndexMut};
use ::std::cmp::{PartialEq, Eq};
use ::std::hash::{Hash, Hasher};
use ::common::INF;

use x86intrin::*;

pub type Color = Vec3;

#[derive(Clone, Copy, Debug)]
pub struct Vec3 {
    data: [f64; 3],
}

impl Vec3 {
    pub fn new(_x: f64, _y: f64, _z: f64) -> Vec3 {
        Vec3 {
            data: [_x, _y, _z],
        }
    }

    pub fn pos_inf() -> Vec3 {
        Vec3::new(INF, INF, INF)
    }

    pub fn neg_inf() -> Vec3 {
        Vec3::new(-INF, -INF, -INF)
    }

    pub fn zero() -> Vec3 {
        Vec3::new(0f64, 0f64, 0f64)
    }

    pub fn x(&self) -> f64 {
        self.data[0]
    }

    pub fn y(&self) -> f64 {
        self.data[1]
    }

    pub fn z(&self) -> f64 {
        self.data[2]
    }

    pub fn dot(v1: &Vec3, v2: &Vec3) -> f64 {
        v1.x() * v2.x() + v1.y() * v2.y() + v1.z() * v2.z()
    }

    pub fn cross(v1: &Vec3, v2: &Vec3) -> Vec3 {
        Vec3::new(
            v1.y() * v2.z() - v1.z() * v2.y(),
            v1.z() * v2.x() - v1.x() * v2.z(),
            v1.x() * v2.y() - v1.y() * v2.x(),
        )
    }

    pub fn norm(&self) -> f64 {
        Vec3::dot(&self, &self).sqrt()
    }

    pub fn normalize(self) -> Vec3 {
        let n = self.norm();
        self / n
    }

    pub fn max(&self, other: &Vec3) -> Vec3 {
        Vec3::new(f64::max(self.x(), other.x()), f64::max(self.y(), other.y()), f64::max(self.z(), other.z()))
    }

    pub fn min(&self, other: &Vec3) -> Vec3 {
        Vec3::new(f64::min(self.x(), other.x()), f64::min(self.y(), other.y()), f64::min(self.z(), other.z()))
    }

    pub fn max_elements(&self) -> f64 {
        f64::max(f64::max(self.x(), self.y()), self.z())
    }

    pub fn min_elements(&self) -> f64 {
        f64::min(f64::min(self.x(), self.y()), self.z())
    }

    pub fn abs(&self) -> Vec3 {
        Vec3::new(self.x().abs(), self.y().abs(), self.z().abs())
    }
}

impl PartialEq for Vec3 {
    fn eq(&self, v: &Vec3) -> bool {
        self.x() == v.x() && self.y() == v.y() && self.z() == v.z()
    }
}

impl Eq for Vec3 {}

impl Hash for Vec3 {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.x().to_bits().hash(state);
        self.y().to_bits().hash(state);
        self.z().to_bits().hash(state);
    }
}

impl Index<usize> for Vec3 {
    type Output = f64;

    fn index(&self, index: usize) -> &Self::Output {
        &self.data[index]
    }
}

impl IndexMut<usize> for Vec3 {
    fn index_mut(&mut self, index: usize) -> &mut f64 {
        &mut self.data[index]
    }
}

impl Add for Vec3 {
    type Output = Vec3;

    fn add(self, v: Vec3) -> Self::Output {
        Vec3::new(self.x() + v.x(), self.y() + v.y(), self.z() + v.z())
    }
}

impl Add<f64> for Vec3 {
    type Output = Vec3;

    fn add(self, v: f64) -> Self::Output {
        Vec3::new(self.x() + v, self.y() + v, self.z() + v)
    }
}

impl Sub for Vec3 {
    type Output = Vec3;

    fn sub(self, v: Vec3) -> Self::Output {
        Vec3::new(self.x() - v.x(), self.y() - v.y(), self.z() - v.z())
    }
}

impl Sub<f64> for Vec3 {
    type Output = Vec3;

    fn sub(self, v: f64) -> Self::Output {
        Vec3::new(self.x() - v, self.y() - v, self.z() - v)
    }
}

impl Mul for Vec3 {
    type Output = Vec3;

    fn mul(self, v: Vec3) -> Self::Output {
        Vec3::new(self.x() * v.x(), self.y() * v.y(), self.z() * v.z())
    }
}

impl Mul<f64> for Vec3 {
    type Output = Vec3;

    fn mul(self, v: f64) -> Self::Output {
        Vec3::new(self.x() * v, self.y() * v, self.z() * v)
    }
}

impl Div for Vec3 {
    type Output = Vec3;

    fn div(self, v: Vec3) -> Self::Output {
        Vec3::new(self.x() / v.x(), self.y() / v.y(), self.z() / v.z())
    }
}

impl Div<f64> for Vec3 {
    type Output = Vec3;

    fn div(self, v: f64) -> Self::Output {
        Vec3::new(self.x() / v, self.y() / v, self.z() / v)
    }
}

impl AddAssign for Vec3 {
    fn add_assign(&mut self, v: Vec3) {
        for i in 0..3 {
            self.data[i] += v.data[i];
        }
    }
}

impl AddAssign<f64> for Vec3 {
    fn add_assign(&mut self, v: f64) {
        for i in 0..3 {
            self.data[i] += v;
        }
    }
}

impl SubAssign for Vec3 {
    fn sub_assign(&mut self, v: Vec3) {
        for i in 0..3 {
            self.data[i] -= v.data[i];
        }
    }
}

impl SubAssign<f64> for Vec3 {
    fn sub_assign(&mut self, v: f64) {
        for i in 0..3 {
            self.data[i] -= v;
        }
    }
}

impl MulAssign for Vec3 {
    fn mul_assign(&mut self, v: Vec3) {
        for i in 0..3 {
            self.data[i] *= v.data[i];
        }
    }
}

impl MulAssign<f64> for Vec3 {
    fn mul_assign(&mut self, v: f64) {
        for i in 0..3 {
            self.data[i] *= v;
        }
    }
}

impl DivAssign for Vec3 {
    fn div_assign(&mut self, v: Vec3) {
        for i in 0..3 {
            self.data[i] /= v.data[i];
        }
    }
}

impl DivAssign<f64> for Vec3 {
    fn div_assign(&mut self, v: f64) {
        for i in 0..3 {
            self.data[i] /= v;
        }
    }
}

impl Neg for Vec3 {
    type Output = Vec3;

    fn neg(self) -> Self::Output {
        Vec3::new(-self.x(), -self.y(), -self.z())
    }
}

#[derive(Debug)]
pub struct Vec3x4 {
    pub xs: f64x4,
    pub ys: f64x4,
    pub zs: f64x4,
}

impl Vec3x4 {
    pub fn new(v0: Vec3, v1: Vec3, v2: Vec3, v3: Vec3) -> Vec3x4 {
        Vec3x4 {
            xs: mm256_setr_pd(v0.x(), v1.x(), v2.x(), v3.x()).as_f64x4(),
            ys: mm256_setr_pd(v0.y(), v1.y(), v2.y(), v3.y()).as_f64x4(),
            zs: mm256_setr_pd(v0.z(), v1.z(), v2.z(), v3.z()).as_f64x4(),
        }
    }
}

#[cfg(test)]
mod vec3_tests {
    use ::math::vec3::Vec3;

    #[test]
    fn test_new() {
        let v1 = Vec3::new(0f64, 1f64, 2f64);
        assert_eq!(v1.x(), 0f64);
        assert_eq!(v1.y(), 1f64);
        assert_eq!(v1.z(), 2f64);
    }

    #[test]
    fn test_index() {
        let v1 = Vec3::new(0f64, 1f64, 2f64);

        for i in 0..3 {
            assert_eq!(v1[i] as usize, i);
        }
    }

    #[test]
    fn test_index_mut() {
        let mut v1 = Vec3::new(0f64, 1f64, 2f64);

        for i in 0..3 {
            assert_eq!(v1[i] as usize, i);
        }
    }


    #[test]
    fn test_add() {
        let v1 = Vec3::new(0f64, 1f64, 2f64);
        let v2 = Vec3::new(10f64, 11f64, 12f64);
        let v3 = v1 + v2;

        assert_eq!(v3.x(), 10f64);
        assert_eq!(v3.y(), 12f64);
        assert_eq!(v3.z(), 14f64);

        let v4 = v3 + 1f64;

        assert_eq!(v4.x(), 11f64);
        assert_eq!(v4.y(), 13f64);
        assert_eq!(v4.z(), 15f64);
    }

    #[test]
    fn test_sub() {
        let v1 = Vec3::new(0f64, 1f64, 2f64);
        let v2 = Vec3::new(10f64, 11f64, 12f64);
        let v3 = v1 - v2;

        assert_eq!(v3.x(), -10f64);
        assert_eq!(v3.y(), -10f64);
        assert_eq!(v3.z(), -10f64);

        let v4 = v3 - 1f64;

        assert_eq!(v4.x(), -11f64);
        assert_eq!(v4.y(), -11f64);
        assert_eq!(v4.z(), -11f64);
    }

    #[test]
    fn test_mul() {
        let v1 = Vec3::new(0f64, 1f64, 2f64);
        let v2 = Vec3::new(10f64, 11f64, 12f64);
        let v3 = v1 * v2;

        assert_eq!(v3.x(), 0f64);
        assert_eq!(v3.y(), 11f64);
        assert_eq!(v3.z(), 24f64);

        let v4 = v3 * 2f64;

        assert_eq!(v4.x(), 0f64);
        assert_eq!(v4.y(), 22f64);
        assert_eq!(v4.z(), 48f64);
    }

    #[test]
    fn test_div() {
        let v1 = Vec3::new(0f64, 1f64, 2f64);
        let v2 = Vec3::new(10f64, 11f64, 12f64);
        let v3 = v1 / v2;

        assert_eq!(v3.x(), 0f64);
        assert_eq!(v3.y(), 1f64 / 11f64);
        assert_eq!(v3.z(), 2f64 / 12f64);

        let v4 = v3 / 2f64;

        assert_eq!(v4.x(), 0f64);
        assert_eq!(v4.y(), 1f64 / 22f64);
        assert_eq!(v4.z(), 2f64 / 24f64);
    }

    #[test]
    fn test_add_assign() {
        let mut v1 = Vec3::new(0f64, 1f64, 2f64);
        let v2 = Vec3::new(10f64, 11f64, 12f64);
        v1 += v2;

        assert_eq!(v1.x(), 10f64);
        assert_eq!(v1.y(), 12f64);
        assert_eq!(v1.z(), 14f64);

        v1 += 1f64;

        assert_eq!(v1.x(), 11f64);
        assert_eq!(v1.y(), 13f64);
        assert_eq!(v1.z(), 15f64);
    }

    #[test]
    fn test_sub_assign() {
        let mut v1 = Vec3::new(0f64, 1f64, 2f64);
        let v2 = Vec3::new(10f64, 11f64, 12f64);
        v1 -= v2;

        assert_eq!(v1.x(), -10f64);
        assert_eq!(v1.y(), -10f64);
        assert_eq!(v1.z(), -10f64);

        v1 -= 1f64;

        assert_eq!(v1.x(), -11f64);
        assert_eq!(v1.y(), -11f64);
        assert_eq!(v1.z(), -11f64);
    }

    #[test]
    fn test_mul_assign() {
        let mut v1 = Vec3::new(0f64, 1f64, 2f64);
        let v2 = Vec3::new(10f64, 11f64, 12f64);
        v1 *= v2;

        assert_eq!(v1.x(), 0f64);
        assert_eq!(v1.y(), 11f64);
        assert_eq!(v1.z(), 24f64);

        v1 *= 2f64;

        assert_eq!(v1.x(), 0f64);
        assert_eq!(v1.y(), 22f64);
        assert_eq!(v1.z(), 48f64);
    }

    #[test]
    fn test_div_assign() {
        let mut v1 = Vec3::new(0f64, 1f64, 2f64);
        let v2 = Vec3::new(10f64, 11f64, 12f64);
        v1 /= v2;

        assert_eq!(v1.x(), 0f64);
        assert_eq!(v1.y(), 1f64 / 11f64);
        assert_eq!(v1.z(), 2f64 / 12f64);

        v1 /= 2f64;

        assert_eq!(v1.x(), 0f64);
        assert_eq!(v1.y(), 1f64 / 22f64);
        assert_eq!(v1.z(), 2f64 / 24f64);
    }

    #[test]
    fn test_neg() {
        let v1 = Vec3::new(0f64, 1f64, 2f64);
        let v2 = -v1;

        assert_eq!(v2.x(), 0f64);
        assert_eq!(v2.y(), -1f64);
        assert_eq!(v2.z(), -2f64);
    }

    #[test]
    fn test_dot() {
        let v1 = Vec3::new(1f64, 2f64, 3f64);
        let v2 = Vec3::new(1f64, 2f64, 3f64);
        let d = Vec3::dot(&v1, &v2);
        assert_eq!(d, 14f64);
    }

    #[test]
    fn test_cross() {
        let v1 = Vec3::new(1f64, 2f64, 3f64);
        let v2 = Vec3::new(3f64, 4f64, 5f64);
        let v3 = Vec3::cross(&v1, &v2);

        assert!(Vec3::dot(&v1, &v3).abs() < 1e-7);
        assert!(Vec3::dot(&v2, &v3).abs() < 1e-7);
    }

    #[test]
    fn test_norm() {
        let v1 = Vec3::new(1f64, 2f64, 3f64);
        let n = v1.norm();

        assert_eq!(n, 14f64.sqrt())
    }

    #[test]
    fn test_normalize() {
        let v1 = Vec3::new(1f64, 2f64, 3f64).normalize();

        assert!((v1.norm() - 1f64).abs() < 1e-7)
    }

    #[test]
    fn test_max_elements() {
        let v1 = Vec3::new(1f64, 2f64, 3f64);
        assert!(v1.max_elements() == 3f64);
    }

    #[test]
    fn test_min_elements() {
        let v1 = Vec3::new(1f64, 2f64, 3f64);
        assert!(v1.min_elements() == 1f64);
    }

    #[test]
    fn test_max() {
        let v1 = Vec3::new(1f64, 2f64, 3f64);
        let v2 = Vec3::new(-1f64, 4f64, 2f64);
        let v3 = v1.max(&v2);
        assert_eq!(v3.x(), 1f64);
        assert_eq!(v3.y(), 4f64);
        assert_eq!(v3.z(), 3f64);
    }

        #[test]
    fn test_min() {
        let v1 = Vec3::new(1f64, 2f64, 3f64);
        let v2 = Vec3::new(-1f64, 4f64, 2f64);
        let v3 = v1.min(&v2);
        assert_eq!(v3.x(), -1f64);
        assert_eq!(v3.y(), 2f64);
        assert_eq!(v3.z(), 2f64);
    }
}