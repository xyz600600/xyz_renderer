use ::math::vec3::Vec3;
use ::ray::Ray;

use std::ops::{Index, IndexMut};

#[derive(Debug)]
pub struct Matrix4x4 {
    coefs: [[f64; 4]; 4],
}

impl Matrix4x4 {
    pub fn new(_coefs: [[f64; 4]; 4]) -> Matrix4x4 {
        Matrix4x4 {
            coefs: _coefs,
        }
    }

    pub fn translate(diff: &Vec3) -> Matrix4x4 {
        Matrix4x4::new([
            [1f64, 0f64, 0f64, diff.x()],
            [0f64, 1f64, 0f64, diff.y()],
            [0f64, 0f64, 1f64, diff.z()],
            [0f64, 0f64, 0f64, 1f64],
        ])
    }

    pub fn scale(_scale: &Vec3) -> Matrix4x4 {
        Matrix4x4::new([
            [_scale.x(), 0f64, 0f64, 0f64],
            [0f64, _scale.y(), 0f64, 0f64],
            [0f64, 0f64, _scale.z(), 0f64],
            [0f64, 0f64, 0f64, 1f64],
        ])
    }

    // 位置や回転だけを行うもので、特にスケールなどは変わらない点に注意
    pub fn look_at(pos: &Vec3, dir: &Vec3, up: &Vec3) -> Matrix4x4 {
        let right = -Vec3::cross(&up, &dir);
        Matrix4x4::new([
            [right.x(), up.x(), dir.x(), pos.x()],
            [right.y(), up.y(), dir.y(), pos.y()],
            [right.z(), up.z(), dir.z(), pos.z()],
            [0f64, 0f64, 0f64, 1f64]
        ])
    }

    pub fn apply(&self, v: Vec3) -> Vec3 {
        let x = self.coefs[0][0] * v.x() + self.coefs[0][1] * v.y() + self.coefs[0][2] * v.z() + self.coefs[0][3];
        let y = self.coefs[1][0] * v.x() + self.coefs[1][1] * v.y() + self.coefs[1][2] * v.z() + self.coefs[1][3];
        let z = self.coefs[2][0] * v.x() + self.coefs[2][1] * v.y() + self.coefs[2][2] * v.z() + self.coefs[2][3];
        Vec3::new(x, y, z)
    }

    // a.compose(b) -> a \cdot b になる
    // a(b(v)) = (a \cdot b)(v) 
    pub fn compose(self, mat: Matrix4x4) -> Matrix4x4 {
        let mut ret = Matrix4x4 { coefs: [[0f64; 4], [0f64; 4], [0f64; 4], [0f64; 4]] };

        for i in 0..4 {
            for j in 0..4 {
                for k in 0..4 {
                    ret[i][j] += self[i][k] * mat[k][j];
                }
            }
        }
        ret
    }
}

impl Index<usize> for Matrix4x4 {
    type Output = [f64; 4];

    fn index(&self, idx: usize) -> &Self::Output {
        &self.coefs[idx]
    }
}

impl IndexMut<usize> for Matrix4x4 {
    fn index_mut(&mut self, idx: usize) -> &mut [f64; 4] {
        &mut self.coefs[idx]
    }
}

#[test]
fn test_matrix4x4_new() {
    let t = Matrix4x4::new([
        [1f64, 0f64, 0f64, 1f64],
        [0f64, 1f64, 0f64, 1f64],
        [0f64, 0f64, 1f64, 1f64],
        [0f64, 0f64, 0f64, 1f64],
    ]);

    for i in 0..4 {
        for j in 0..4 {
            if i == j || j == 3 {
                assert_eq!(t[i][j], 1f64);
            } else {
                assert_eq!(t[i][j], 0f64);
            }
        }
    }
}

#[test]
fn test_matrix4x4_translate() {
    let t = Matrix4x4::translate(&Vec3::new(1f64, 2f64, 3f64));

    let v = Vec3::new(10f64, 11f64, 12f64);
    let v2 = t.apply(v);

    assert_eq!(v2.x(), 11f64);
    assert_eq!(v2.y(), 13f64);
    assert_eq!(v2.z(), 15f64);
}

#[test]
fn test_matrix4x4_scale() {
    let t = Matrix4x4::scale(&Vec3::new(1f64, 2f64, 3f64));

    let v = Vec3::new(1f64, 2f64, 3f64);
    let v2 = t.apply(v);

    assert_eq!(v2.x(), 1f64);
    assert_eq!(v2.y(), 4f64);
    assert_eq!(v2.z(), 9f64);    
}

#[test]
fn test_matrix4x4_compose() {
    let t1 = Matrix4x4::scale(&Vec3::new(1f64, 2f64, 3f64));
    let t2 = Matrix4x4::translate(&Vec3::new(1f64, 2f64, 3f64));

    let v = Vec3::new(1f64, 2f64, 3f64);
    let v2 = t1.apply(t2.apply(v));

    let t3 = t1.compose(t2);
    let v3 = t3.apply(v);

    assert_eq!(v2.x(), v3.x());
    assert_eq!(v2.y(), v3.y());
    assert_eq!(v2.z(), v3.z());
}