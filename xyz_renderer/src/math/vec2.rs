use ::math::vec3::Vec3;

use std::ops::{Add, Sub, Mul, Div, AddAssign, SubAssign, MulAssign, DivAssign, Neg, Index, IndexMut};
use ::std::cmp::{PartialEq, Eq};
use ::std::hash::{Hash, Hasher};

use std::convert::Into;

#[derive(Clone, Copy, Debug)]
pub struct Vec2 {
    data: [f64; 2],
}

impl Vec2 {
    pub fn new(_x: f64, _y: f64) -> Vec2 {
        Vec2 {
            data: [_x, _y],
        }
    }

    pub fn zero() -> Vec2 {
        Vec2::new(0f64, 0f64)
    }

    pub fn x(&self) -> f64 {
        self.data[0]
    }

    pub fn y(&self) -> f64 {
        self.data[1]
    }

    pub fn dot(v1: &Vec2, v2: &Vec2) -> f64 {
        v1.x() * v2.x() + v1.y() * v2.y()
    }

    pub fn norm(&self) -> f64 {
        Vec2::dot(&self, &self).sqrt()
    }

    pub fn normalize(self) -> Vec2 {
        let n = self.norm();
        self / n
    }
}

impl Into<Vec3> for Vec2 {
    fn into(self) -> Vec3 {
        Vec3::new(self.x(), self.y(), 0f64)
    }
}

impl PartialEq for Vec2 {
    fn eq(&self, v: &Vec2) -> bool {
        self.x() == v.x() && self.y() == v.y()
    }
}

impl Eq for Vec2 {}

impl Hash for Vec2 {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.x().to_bits().hash(state);
        self.y().to_bits().hash(state);
    }
}

impl Index<usize> for Vec2 {
    type Output = f64;

    fn index(&self, index: usize) -> &Self::Output {
        &self.data[index]
    }
}

impl IndexMut<usize> for Vec2 {
    fn index_mut(&mut self, index: usize) -> &mut f64 {
        &mut self.data[index]
    }
}

impl Add for Vec2 {
    type Output = Vec2;

    fn add(self, v: Vec2) -> Self::Output {
        Vec2::new(self.x() + v.x(), self.y() + v.y())
    }
}

impl Add<f64> for Vec2 {
    type Output = Vec2;

    fn add(self, v: f64) -> Self::Output {
        Vec2::new(self.x() + v, self.y() + v)
    }
}

impl Sub for Vec2 {
    type Output = Vec2;

    fn sub(self, v: Vec2) -> Self::Output {
        Vec2::new(self.x() - v.x(), self.y() - v.y())
    }
}

impl Sub<f64> for Vec2 {
    type Output = Vec2;

    fn sub(self, v: f64) -> Self::Output {
        Vec2::new(self.x() - v, self.y() - v)
    }
}

impl Mul for Vec2 {
    type Output = Vec2;

    fn mul(self, v: Vec2) -> Self::Output {
        Vec2::new(self.x() * v.x(), self.y() * v.y())
    }
}

impl Mul<f64> for Vec2 {
    type Output = Vec2;

    fn mul(self, v: f64) -> Self::Output {
        Vec2::new(self.x() * v, self.y() * v)
    }
}

impl Div for Vec2 {
    type Output = Vec2;

    fn div(self, v: Vec2) -> Self::Output {
        Vec2::new(self.x() / v.x(), self.y() / v.y())
    }
}

impl Div<f64> for Vec2 {
    type Output = Vec2;

    fn div(self, v: f64) -> Self::Output {
        Vec2::new(self.x() / v, self.y() / v)
    }
}

impl AddAssign for Vec2 {
    fn add_assign(&mut self, v: Vec2) {
        for i in 0..2 {
            self.data[i] += v.data[i];
        }
    }
}

impl AddAssign<f64> for Vec2 {
    fn add_assign(&mut self, v: f64) {
        for i in 0..2 {
            self.data[i] += v;
        }
    }
}

impl SubAssign for Vec2 {
    fn sub_assign(&mut self, v: Vec2) {
        for i in 0..2 {
            self.data[i] -= v.data[i];
        }
    }
}

impl SubAssign<f64> for Vec2 {
    fn sub_assign(&mut self, v: f64) {
        for i in 0..2 {
            self.data[i] -= v;
        }
    }
}

impl MulAssign for Vec2 {
    fn mul_assign(&mut self, v: Vec2) {
        for i in 0..2 {
            self.data[i] *= v.data[i];
        }
    }
}

impl MulAssign<f64> for Vec2 {
    fn mul_assign(&mut self, v: f64) {
        for i in 0..2 {
            self.data[i] *= v;
        }
    }
}

impl DivAssign for Vec2 {
    fn div_assign(&mut self, v: Vec2) {
        for i in 0..2 {
            self.data[i] /= v.data[i];
        }
    }
}

impl DivAssign<f64> for Vec2 {
    fn div_assign(&mut self, v: f64) {
        for i in 0..2 {
            self.data[i] /= v;
        }
    }
}

impl Neg for Vec2 {
    type Output = Vec2;

    fn neg(self) -> Self::Output {
        Vec2::new(-self.x(), -self.y())
    }
}

#[cfg(test)]
mod vec2_tests {
    use ::math::vec2::Vec2;

    #[test]
    fn test_new() {
        let v1 = Vec2::new(0f64, 1f64);
        assert_eq!(v1.x(), 0f64);
        assert_eq!(v1.y(), 1f64);
    }

    #[test]
    fn test_index() {
        let v1 = Vec2::new(0f64, 1f64);

        for i in 0..2 {
            assert_eq!(v1[i] as usize, i);
        }
    }

    #[test]
    fn test_index_mut() {
        let mut v1 = Vec2::new(0f64, 1f64);

        for i in 0..2 {
            assert_eq!(v1[i] as usize, i);
        }
    }

    #[test]
    fn test_add() {
        let v1 = Vec2::new(0f64, 1f64);
        let v2 = Vec2::new(10f64, 11f64);
        let v3 = v1 + v2;

        assert_eq!(v3.x(), 10f64);
        assert_eq!(v3.y(), 12f64);

        let v4 = v3 + 1f64;

        assert_eq!(v4.x(), 11f64);
        assert_eq!(v4.y(), 13f64);
    }

    #[test]
    fn test_sub() {
        let v1 = Vec2::new(0f64, 1f64);
        let v2 = Vec2::new(10f64, 11f64);
        let v3 = v1 - v2;

        assert_eq!(v3.x(), -10f64);
        assert_eq!(v3.y(), -10f64);

        let v4 = v3 - 1f64;

        assert_eq!(v4.x(), -11f64);
        assert_eq!(v4.y(), -11f64);
    }

    #[test]
    fn test_mul() {
        let v1 = Vec2::new(0f64, 1f64);
        let v2 = Vec2::new(10f64, 11f64);
        let v3 = v1 * v2;

        assert_eq!(v3.x(), 0f64);
        assert_eq!(v3.y(), 11f64);

        let v4 = v3 * 2f64;

        assert_eq!(v4.x(), 0f64);
        assert_eq!(v4.y(), 22f64);
    }

    #[test]
    fn test_div() {
        let v1 = Vec2::new(0f64, 1f64);
        let v2 = Vec2::new(10f64, 11f64);
        let v3 = v1 / v2;

        assert_eq!(v3.x(), 0f64);
        assert_eq!(v3.y(), 1f64 / 11f64);

        let v4 = v3 / 2f64;

        assert_eq!(v4.x(), 0f64);
        assert_eq!(v4.y(), 1f64 / 22f64);
    }

    #[test]
    fn test_add_assign() {
        let mut v1 = Vec2::new(0f64, 1f64);
        let v2 = Vec2::new(10f64, 11f64);
        v1 += v2;

        assert_eq!(v1.x(), 10f64);
        assert_eq!(v1.y(), 12f64);

        v1 += 1f64;

        assert_eq!(v1.x(), 11f64);
        assert_eq!(v1.y(), 13f64);
    }

    #[test]
    fn test_sub_assign() {
        let mut v1 = Vec2::new(0f64, 1f64);
        let v2 = Vec2::new(10f64, 11f64);
        v1 -= v2;

        assert_eq!(v1.x(), -10f64);
        assert_eq!(v1.y(), -10f64);

        v1 -= 1f64;

        assert_eq!(v1.x(), -11f64);
        assert_eq!(v1.y(), -11f64);
    }

    #[test]
    fn test_mul_assign() {
        let mut v1 = Vec2::new(0f64, 1f64);
        let v2 = Vec2::new(10f64, 11f64);
        v1 *= v2;

        assert_eq!(v1.x(), 0f64);
        assert_eq!(v1.y(), 11f64);

        v1 *= 2f64;

        assert_eq!(v1.x(), 0f64);
        assert_eq!(v1.y(), 22f64);
    }

    #[test]
    fn test_div_assign() {
        let mut v1 = Vec2::new(0f64, 1f64);
        let v2 = Vec2::new(10f64, 11f64);
        v1 /= v2;

        assert_eq!(v1.x(), 0f64);
        assert_eq!(v1.y(), 1f64 / 11f64);

        v1 /= 2f64;

        assert_eq!(v1.x(), 0f64);
        assert_eq!(v1.y(), 1f64 / 22f64);
    }

    #[test]
    fn test_neg() {
        let v1 = Vec2::new(0f64, 1f64);
        let v2 = -v1;

        assert_eq!(v2.x(), 0f64);
        assert_eq!(v2.y(), -1f64);
    }

    #[test]
    fn test_dot() {
        let v1 = Vec2::new(1f64, 2f64);
        let v2 = Vec2::new(1f64, 2f64);
        let d = Vec2::dot(&v1, &v2);
        assert_eq!(d, 5f64);
    }

    #[test]
    fn test_norm() {
        let v1 = Vec2::new(1f64, 2f64);
        let n = v1.norm();

        assert_eq!(n, 5f64.sqrt())
    }

    #[test]
    fn test_normalize() {
        let v1 = Vec2::new(1f64, 2f64).normalize();

        assert!((v1.norm() - 1f64).abs() < 1e-7)
    }
}