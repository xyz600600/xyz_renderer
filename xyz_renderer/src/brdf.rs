use math::vec3::Vec3;
use random::Random;
use std::f64::consts::PI;
use ray::Ray;

#[derive(Clone, Copy)]
pub enum ReflectionType {
    Diffuse,
    Specular, 
    Refraction(f64),
    Mixture(f64, f64, f64, f64), // Diffuse 率、Specular 率、Refraction 率、Ni
}

impl ReflectionType {

    // Ray と Ray の接続に使う
    // 単位は立体角である点に注意
    pub fn get_brdf(pat: ReflectionType, ray_in: &Ray, ray_out: &Ray, norm: &Vec3) -> f64 {
        match pat {
            ReflectionType::Diffuse => {
                1f64 / (2f64 * PI)
            },
            ReflectionType::Refraction(ni) => {
                // デルタ関数なので、実用上 0 を返しても問題ない
                0f64
            },
            ReflectionType::Specular => {
                // デルタ関数なので実用上 0 を返しても問題ない
                0f64
            },
            ReflectionType::Mixture(rd, rs, rt, ni) => {
                // Diffuse 成分だけ返す
                rd / ((rd + rs + rt) * 2f64 * PI)
            },
        }
    }

    pub fn next_ray(pat: ReflectionType,
                    prev_ray: &Ray,
                    pos: Vec3,
                    norm: Vec3,
                    rand: &mut Random) -> (f64, f64, Ray) {
        match pat {
            ReflectionType::Diffuse => ReflectionType::next_ray_diffuse(norm, pos, &prev_ray, rand),
            ReflectionType::Refraction(ni) => ReflectionType::next_ray_refraction(norm, pos, &prev_ray, rand, ni),
            ReflectionType::Specular => ReflectionType::next_ray_specular(norm, pos, &prev_ray),
            ReflectionType::Mixture(rd, rs, rr, ni) => {
                let sel = rand.gen_range(0f64, rd + rs + rr);
                if sel < rd {
                    ReflectionType::next_ray_diffuse(norm, pos, &prev_ray, rand)
                } else if rd <= sel && sel < rd + rs {
                    ReflectionType::next_ray_specular(norm, pos, &prev_ray)
                } else {
                    ReflectionType::next_ray_refraction(norm, pos, &prev_ray, rand, ni)
                }
            },
        }
    }

    fn next_ray_specular(norm: Vec3, pos: Vec3, prev_ray: &Ray) -> (f64, f64, Ray) {
        let ver = norm * Vec3::dot(&norm, &prev_ray.dir);
        (1f64, 1f64, Ray::new(pos, (prev_ray.dir - ver * 2f64).normalize(), prev_ray.ni))
    }

    fn next_ray_diffuse(norm: Vec3, pos: Vec3, prev_ray: &Ray, rand: &mut Random) -> (f64, f64, Ray) {
        let z = rand.gen_range(-1f64, 1f64);
        let phi = rand.gen_range(0f64, PI * 2f64);
        let xy_norm = (1f64 - z * z).sqrt();
        let ret = Vec3::new(xy_norm * phi.cos(), xy_norm * phi.sin(), z);
        let ret = if Vec3::dot(&ret, &norm) < 0f64 {
            -ret
        } else {
            ret
        };
        let dist = 1f64 / (PI * 2f64);
        (dist, dist, Ray::new(pos, ret, prev_ray.ni))
    }

    fn next_ray_refraction(norm: Vec3, pos: Vec3, prev_ray: &Ray, rand: &mut Random, ni: f64) -> (f64, f64, Ray) {
        
        let ni_in = prev_ray.ni;
        let ni_out = if ni_in == ni {
            1f64
        } else {
            ni
        };

        let rel_ni = ni_in / ni_out;
        let cos_in = Vec3::dot(&prev_ray.dir, &norm).abs();
        let sin_in = (1f64 - cos_in * cos_in).sqrt();
        let sin_out = rel_ni * sin_in;
        let cos_out = (1f64 - sin_out * sin_out).sqrt();

        if sin_out >= 1f64 {
            ReflectionType::next_ray_specular(norm, pos, prev_ray)
        } else {
            let f0 = ((ni_in - ni_out) / (ni_in + ni_out)).powi(2);
            let fr = f0 + (1f64 - f0) * (1f64 - cos_in.min(cos_out)).powi(5);

            // とりあえず importance sampling しない
            if rand.gen_range(0f64, 1f64) <= fr {
                ReflectionType::next_ray_specular(norm, pos, prev_ray)
            } else {
                let dir_out = (prev_ray.dir * rel_ni - norm * (rel_ni * Vec3::dot(&prev_ray.dir, &norm) + cos_out)).normalize();
                (1f64, 1f64, Ray::new(pos, dir_out, ni_out))
            }
        }
    }
}


#[test]
fn test_diffuse_nextdir() {

    let in_dir = Vec3::new(0f64, 1f64, 2f64).normalize();
    let pos = Vec3::new(0f64, 0f64, 0f64);
    let ray = Ray::new(pos, in_dir, 1f64);
    let norm = Vec3::new(1f64, 1f64, 1f64).normalize();

    let mut rand = Random::new();

    for _ in 0..1000 {
        let ret = ReflectionType::next_ray(ReflectionType::Diffuse, &ray, pos, norm, &mut rand).2.dir;
        assert!(Vec3::dot(&ret, &norm) > 0f64);
    }
}

#[test] 
fn test_specular_nextdir() {

    use common::EPS;

    let in_dir = Vec3::new(-1f64, 0f64, -1f64).normalize();
    let pos = Vec3::new(0f64, 0f64, 0f64);
    let norm = Vec3::new(0f64, 0f64, 1f64);
    let ray = Ray::new(pos, in_dir, 1f64);
    let mut rand = Random::new();

    let ret = ReflectionType::next_ray(ReflectionType::Specular, &ray, pos, norm, &mut rand).2.dir;

    assert!((0.5f64.sqrt() + ret.x()).abs() < EPS);
    assert!(ret.y().abs() < EPS);
    assert!((-(0.5f64.sqrt()) + ret.z()).abs() < EPS);
}