
const MAX_SIZE: usize = 100;

pub struct FastStack<T> {
    data: [T; MAX_SIZE],
    _size: usize,
}

impl<T: Default + Copy + Clone> FastStack<T> {

    pub fn new() -> FastStack<T> {
        FastStack {
            data: [T::default(); MAX_SIZE],
            _size: 0,            
        }
    }

    pub fn push(&mut self, val: T) {
        self.data[self._size] = val;
        self._size += 1;
    }

    pub fn pop(&mut self) -> Option<T> {
        if self._size == 0 {
            None
        } else {
            self._size -= 1;
            Some(self.data[self._size])
        }
    } 

    pub fn len(&self) -> usize {
        self._size
    }
}