use ::object::rectangle::AABB;
use ::object::{Object, Intersectable, Primitive, Material};
use ::math::vec3::{Vec3, Color};
use ::brdf::ReflectionType;
use ::texture::Texture;
use ::ray::Ray;
use ::common::{INF, EPS};
use ::random::Random;

use std::f64::consts::PI;

pub struct Sphere {
    center: Vec3,
    radius: f64,
    state: Material,
}

impl Sphere {
    pub fn new(_center: Vec3, _radius: f64, _state: Material) -> Sphere {
        Sphere {
            center: _center,
            radius: _radius,
            state: _state,
        }
    }
}

impl Primitive for Sphere {

    fn normal(&self, pos: Vec3) -> Vec3 {
        (pos - self.center).normalize()
    }
}

impl Intersectable for Sphere {
    fn intersect(&self, ray: &Ray) -> Option<(&Object, &Primitive, f64)> {

        let k = self.center - ray.pos;
        let b = -Vec3::dot(&k, &ray.dir);
        let c = Vec3::dot(&k, &k) - self.radius * self.radius;
        let d = b * b - c;

        if d < 0f64 {
            None
        } else {
            let d_sq = d.sqrt();
            let t_min = -b - d_sq;
            let t_max = -b + d_sq;

            if t_min > EPS {
                Some((self, self, t_min))
            } else if t_max > EPS {
                Some((self, self, t_max))
            } else {
                None
            }
        }
    }

    fn bounding_box(&self) -> AABB {
        AABB::new(self.center - self.radius, self.center + self.radius)
    }
}

impl Object for Sphere {
    fn material(&self, _: usize) -> &Material {
        &self.state
    }

    fn light(&self) -> Option<&Primitive> {
        if self.state.emission.max_elements() > 0f64 {
            Some(self)
        } else {
            None
        }
    }

    fn sample(&self, rand: &mut Random) -> (f64, Ray) {
        let z = rand.gen_range(-1f64, 1f64);
        let phi = rand.gen_range(0f64, PI * 2f64);
        let r = (1f64 - z * z).sqrt();
        let dir = Vec3::new(r * phi.cos(), r * phi.sin(), z);
        let pos = self.center + dir * (self.radius + EPS);
        (1f64 / (4f64 * PI * self.radius * self.radius), Ray::new(pos, dir, 1f64))
    }
}

#[test]
fn test_sphere_intersect() {
    use ::texture::monotone_texture::MonotoneTexture;
    use ::math::vec3::Color;

    let sphere = Sphere::new(
        Vec3::new(1f64, 1f64, 1f64), 
        1f64, 
        Material::new(
            Box::new(MonotoneTexture::new(Color::new(0.5f64, 0.5f64, 0.5f64))),
            ReflectionType::Diffuse,
            Vec3::new(0f64, 0f64, 0f64)
        ),
    );
    
    let pos = Vec3::new(0f64, 0f64, 0f64);
    let dir = Vec3::new(1f64, 1f64, 1f64).normalize();
    let ray = Ray::new(pos, dir, 1f64);

    let a = sphere.intersect(&ray);
    let expected_dist = 3f64.sqrt() - 1f64;

    assert!(a.is_some());
    assert!((a.unwrap().2 - expected_dist).abs() < EPS);
}

#[test]
fn test_sphere_normal() {
    use ::texture::monotone_texture::MonotoneTexture;
    use ::math::vec3::Color;

    let sphere = Sphere::new(
        Vec3::new(0f64, 0f64, 0f64), 
        1f64, 
        Material::new(
            Box::new(MonotoneTexture::new(Color::new(0.5f64, 0.5f64, 0.5f64))),
            ReflectionType::Diffuse,
            Vec3::new(0f64, 0f64, 0f64)
        )
    );

    let pos = Vec3::new(0f64, 1f64, 0f64);
    let normal = sphere.normal(pos);

    assert_eq!(normal.x(), 0f64);
    assert_eq!(normal.y(), 1f64);
    assert_eq!(normal.z(), 0f64);
}