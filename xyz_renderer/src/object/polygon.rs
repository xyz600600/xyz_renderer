use ::object::rectangle::AABB;
use ::object::{Object, Intersectable, Material, Primitive};
use ::math::vec3::{Vec3, Color};
use ::brdf::ReflectionType;
use ::texture::Texture;
use ::ray::Ray;
use ::common::{INF, EPS};
use ::random::Random;

pub struct Face {
    orig: Vec3,
    d1: Vec3,
    d2: Vec3,
    _normal: Vec3,
}

impl Face {
    pub fn new(_v1: Vec3, _v2: Vec3, _v3: Vec3) -> Face {
        let _d1 = _v2 - _v1;
        let _d2 = _v3 - _v1;
        let normal = Vec3::cross(&_d1, &_d2).normalize();
        Face {
            orig: _v1,
            d1: _d1,
            d2: _d2,
            _normal: normal,
        }
    }
    
    pub fn intersect_base(orig: &Vec3, d1: &Vec3, d2: &Vec3, normal: &Vec3, ray: &Ray) -> f64 {
        let diff = ray.pos - *orig;
        let t = - Vec3::dot(&diff, &normal) / Vec3::dot(&ray.dir, &normal);
        
        if t > EPS {
            let p = diff + ray.dir * t;
            
            let a = Vec3::dot(&p, &d1);
            let b = Vec3::dot(&d1, &d1);
            let c = Vec3::dot(&d1, &d2);
            let d = Vec3::dot(&p, &d2);
            let e = Vec3::dot(&d2, &d2);

            let denom = c * c - b * e;

            if denom == 0f64 {
                INF
            } else {
                let alpha = (c * d - a * e) / denom;
                let beta  = (a * c - b * d) / denom;

                if 0f64 <= alpha && 0f64 <= beta && alpha + beta <= 1f64 {
                    t
                } else {
                    INF
                }
            }
        } else {
            INF
        }
    }
}

impl Face {

    fn bounding_box(&self) -> AABB {
        let v1 = self.d1 + self.orig;
        let v2 = self.d2 + self.orig;
        let max = self.orig.max(&v1).max(&v2);
        let min = self.orig.min(&v1).min(&v2);
        AABB::new(min, max)
    }

    fn intersect(&self, ray: &Ray) -> f64 {
        Face::intersect_base(&self.orig, &self.d1, &self.d2, &self._normal, &ray)
    }
}

pub struct Polygon {
    face: Face,
    state: Material,
}

impl Polygon {
    pub fn new(_face: Face, _state: Material) -> Polygon {
        Polygon {
            face: _face,
            state: _state,
        }
    }
}

impl Primitive for Polygon {

    fn normal(&self, pos: Vec3) -> Vec3 {
        self.face._normal
    }
}

impl Intersectable for Polygon {
    fn intersect(& self, ray: &Ray) -> Option<(&Object, &Primitive, f64)> {
        let t = self.face.intersect(ray);
        if t > EPS {
            Some((self, self, t))
        } else {
            None
        }
    }

    fn bounding_box(&self) -> AABB {
        self.face.bounding_box()
    }
}

impl Object for Polygon {
    fn material(&self, _: usize) -> &Material {
        &self.state
    }
}

#[test]
fn test_polygon_intersect() {
    use ::texture::monotone_texture::MonotoneTexture;
    use ::math::vec3::Color;

    let polygon = Polygon::new(
        Face::new(
            Vec3::new(1f64, 0f64, 0f64),
            Vec3::new(0f64, 1f64, 0f64),
            Vec3::new(0f64, 0f64, 1f64)
        ), 
        Material::new(
            Box::new(MonotoneTexture::new(Color::new(0.5f64, 0.5f64, 0.5f64))),
            ReflectionType::Diffuse,
            Vec3::new(0f64, 0f64, 0f64)
        ),
    );

    let ray = Ray::new(Vec3::new(0f64, 0f64, 0f64), Vec3::new(1f64, 1f64, 1f64).normalize(), 1f64);

    let (obj, _, dist) = polygon.intersect(&ray).unwrap();

    assert!((1f64 / 3f64.sqrt() - dist).abs() < EPS);
}

#[test]
fn test_polygon_intersect_base() {
    let v1 = Vec3::new(1f64, 1f64, 1f64);
    let v2 = Vec3::new(1f64, 2f64, 1f64);
    let v3 = Vec3::new(1f64, 2f64, 2f64);
    let d1 = v2 - v1;
    let d2 = v3 - v1;
    let norm = Vec3::cross(&d1, &d2).normalize();

    let target_pos = ((v1 + v2 + v3) / 3f64).normalize();
    let ray = Ray::new(Vec3::zero(), target_pos, 1f64);
    let t1 = Face::intersect_base(&v1, &d1, &d2, &norm, &ray);
    assert!(t1 < INF);
}