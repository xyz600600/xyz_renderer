use ::object::{Object, Intersectable, Material, Primitive};
use ::math::vec3::{Vec3, Color};
use ::math::vec2::Vec2;
use ::brdf::ReflectionType;
use ::ray::Ray;
use ::object::MaterialState;
use ::object::rectangle::AABB;
use ::object_manager::bvh::BVH2ModelFace;
use ::object::polygon::Face;
use ::common::INF;
use ::random::Random;

use ::std::vec::{IntoIter, Vec};
use ::std::iter::Peekable;

use ::std::path::Path;

use ::std::fs;
use ::std::io::{BufRead, BufReader};

use ::std::collections::HashMap;

#[derive(Debug, Clone, Copy)]
pub struct ModelFaceData {
    vertex_idx: [usize; 3],
    mtl_idx: usize,
    norm: Vec3,
}

impl ModelFaceData {
    pub fn new(_vertex_idx: [usize; 3], _mtl_idx: usize, _norm: Vec3) -> ModelFaceData {
        ModelFaceData {
            vertex_idx: _vertex_idx,
            mtl_idx: _mtl_idx,
            norm: _norm,
        }
    }
}

#[derive(Clone, Copy)]
pub struct ModelFace {
    data: ModelFaceData,
    vertex_list: *const Vec3,
    texture_vertex_list: *const Vec2,
}

unsafe impl Sync for ModelFace {}
unsafe impl Send for ModelFace {}

impl ModelFace {
    pub fn new(_data: ModelFaceData, _vertex_list: &Vec<Vec3>, _texture_vertex_list: &Vec<Vec2>) -> ModelFace {
        ModelFace {
            data: _data,
            vertex_list: _vertex_list.as_ptr(),
            texture_vertex_list: _texture_vertex_list.as_ptr(),
        }
    }
    pub fn v1(&self) -> Vec3 {
        unsafe {
            *self.vertex_list.offset(self.data.vertex_idx[0] as isize)
        }
    }
    pub fn v2(&self) -> Vec3 {
        unsafe {
            *self.vertex_list.offset(self.data.vertex_idx[1] as isize)
        }
    }
    pub fn v3(&self) -> Vec3 {
        unsafe {
            *self.vertex_list.offset(self.data.vertex_idx[2] as isize)
        }
    }

    pub fn intersect(&self, ray: &Ray) -> f64 {
        let v1 = self.v1();
        let d1 = self.v2() - self.v1();
        let d2 = self.v3() - self.v1();
        
        Face::intersect_base(&v1, &d1, &d2, &self.data.norm, &ray)
    }

    pub fn bounding_box(&self) -> AABB {
        let v1 = self.v1();
        let v2 = self.v2();
        let v3 = self.v3();
        let min = Vec3::pos_inf().min(&v1).min(&v2).min(&v3);
        let max = Vec3::neg_inf().max(&v1).max(&v2).max(&v3);
        AABB::new(min, max)
    }
}

impl Primitive for ModelFace {

    fn normal(&self, pos: Vec3) -> Vec3 {
        self.data.norm
    }

    fn material_id(&self) -> usize {
        self.data.mtl_idx
    }
}

unsafe impl Sync for ModelObjectRef {}
unsafe impl Send for ModelObjectRef {}

pub struct ModelObjectRef {
    mtl_list: Vec<Material>,
    vertex_list: *const Vec3,
    texture_vertex_list: *const Vec2,
    norm_list: *const Vec3,
    obj_manager: *const BVH2ModelFace,
    scale: f64,
    shift: Vec3,
    _bounding_box: AABB,
}

impl Object for ModelObjectRef {
    fn material(&self, index: usize) -> &Material {
        &self.mtl_list[index]
    }
}

impl Intersectable for ModelObjectRef {
    fn bounding_box(&self) -> AABB {
        self._bounding_box
    }

    fn intersect(&self, ray: &Ray) -> Option<(&Object, &Primitive, f64)> {
        let tmp_ray = ray.translate_inv(self.scale, self.shift);

        unsafe {
            match (*self.obj_manager).intersect(&tmp_ray) {
                Some((obj_ref, dist)) => Some((self, obj_ref, dist * self.scale)),
                None => None,
            }
        }
    }
}

pub struct ModelObject {
    mtl_list: Vec<Material>,
    vertex_list: Vec<Vec3>,
    texture_vertex_list: Vec<Vec2>,
    norm_list: Vec<Vec3>,
    obj_manager: BVH2ModelFace,
    scale: f64,
    shift: Vec3,
    _bounding_box: AABB,
}

impl ModelObject {

    pub fn new(_path: &str, scale: f64, shift: Vec3) -> ModelObject {
        let mut ret = ModelObject::load_obj(&_path);
        let inner_bb = ret.obj_manager.bounding_box();
        ret._bounding_box = AABB::new(inner_bb.min * scale + shift, inner_bb.max * scale + shift);
        ret.scale = scale;
        ret.shift = shift;
        ret
    }

    pub fn clone(&self, _scale: f64, _shift: Vec3, mtl_path: &str) -> ModelObjectRef {
        let new_bb = AABB::new(
            (self._bounding_box.min - self.shift) / self.scale * _scale + _shift,
            (self._bounding_box.max - self.shift) / self.scale * _scale + _shift,
        );
        let mut mtl_list = vec![];
        let mut name_list = vec![];
        ModelObject::load_mtl(Path::new(mtl_path), &mut mtl_list, &mut name_list);

        let ret = ModelObjectRef {
            // TODO: 元の Material を Clone する
            mtl_list: mtl_list,
            vertex_list: self.vertex_list.as_ptr(),
            texture_vertex_list: self.texture_vertex_list.as_ptr(),
            norm_list: self.norm_list.as_ptr(),
            obj_manager: &self.obj_manager,
            scale: _scale,
            shift: _shift,
            _bounding_box: new_bb,
        };
        ret
    }

    fn load_single_mtl(path: &Path, lines: &mut Peekable<IntoIter<String>>) -> (String, Material) {
        let mut mtl = MaterialState::new();

        // 最初は newmtl 確定なので、無理やりunwrap + split して取り出す
        let mtl_name = lines.next().unwrap().split_whitespace().collect::<Vec<&str>>()[1].to_string();

        loop {
            if let Some(line) = lines.peek() {
                let split = line.split_whitespace().collect::<Vec<&str>>();
                if split.len() > 0 {
                    match split[0] {
                        "newmtl" => {
                            // 次の newmtl なので、ここで終わり
                            return (mtl_name, mtl.into());
                        },
                        "Tf" => {
                            mtl.tf[0] = split[1].parse().unwrap();
                            mtl.tf[1] = split[2].parse().unwrap();
                            mtl.tf[2] = split[3].parse().unwrap();
                        }
                        "Ka" => {
                            mtl.ka[0] = split[1].parse().unwrap();
                            mtl.ka[1] = split[2].parse().unwrap();
                            mtl.ka[2] = split[3].parse().unwrap();
                        }, 
                        "Kd" => {
                            mtl.kd[0] = split[1].parse().unwrap();
                            mtl.kd[1] = split[2].parse().unwrap();
                            mtl.kd[2] = split[3].parse().unwrap();
                        }, 
                        "Ks" => {
                            mtl.ks[0] = split[1].parse().unwrap();
                            mtl.ks[1] = split[2].parse().unwrap();
                            mtl.ks[2] = split[3].parse().unwrap();
                        }, 
                        "map_Kd" => {
                            let texture_file = split.iter().find(|&&line| line.contains(".jpg") || line.contains(".png")).unwrap();
                            mtl.map_kd.insert_str(0, path.parent().unwrap().join(texture_file).to_str().unwrap().replace("\\", "/").as_str());
                        }, 
                        "map_Ke" => {
                            let texture_file = split.iter().find(|&&line| line.contains(".jpg") || line.contains(".png")).unwrap();
                            mtl.map_ke.insert_str(0, path.parent().unwrap().join(texture_file).to_str().unwrap().replace("\\", "/").as_str());
                        }
                        _ => {},
                    }
                }
            } else {
                break;
            }
            lines.next();
        }
        (mtl_name, mtl.into())
    }

    fn load_mtl(path: &Path, mtl_list: & mut Vec<Material>, name_list: &mut Vec<String>) {
        let fin = BufReader::new(fs::File::open(path).unwrap());
        // 型の複雑化を避けた結果、Vec へ変換
        let mut iters = fin.lines().map(|l| l.unwrap()).collect::<Vec<String>>().into_iter().peekable();

        let is_not_next_newmtl = |line: Option<&String>| {
            match line {
                None => false,
                Some(line) => {
                    !line.starts_with("newmtl")
                },
            }
        };

        while is_not_next_newmtl(iters.peek()) {
            iters.next();
        }

        loop {
            if iters.peek().is_none() {
                break;
            } else {
                let (name, mtl) = ModelObject::load_single_mtl(&path, &mut iters);
                name_list.push(name);
                mtl_list.push(mtl);
            }
        }
    }
 
    fn load_obj(_path: &str) -> ModelObject {
        let path = Path::new(_path);
        let fin = BufReader::new(fs::File::open(path).unwrap());

        let mut model = ModelObject {
            mtl_list: vec![],
            vertex_list: vec![],
            texture_vertex_list: vec![],
            norm_list: vec![], 
            obj_manager: BVH2ModelFace::new(),
            scale: 1f64,
            shift: Vec3::zero(),
            _bounding_box: AABB::new(Vec3::zero(), Vec3::zero()),
        };

        let mut name_list: Vec<String> = vec![];
        let mut mtl_index = 0;

        let mut v_lst: Vec<Vec3> = vec![];
        let mut vt_lst: Vec<Vec2> = vec![];
        let mut vn_lst : Vec<Vec3> = vec![];

        let mut ptr2idx: HashMap<(usize, usize, usize), usize> = HashMap::default();

        let mut data_list: Vec<ModelFaceData> = vec![];

        for line in fin.lines().map(|l| l.unwrap()) {
            let line = line.trim();
            if line.len() > 0 && !line.starts_with("#") {
                let split = line.split_whitespace().collect::<Vec<&str>>();
                match split[0] {
                    "mtllib" => {
                        let mtl_path = path.parent().unwrap().join(Path::new(split[1]));
                        ModelObject::load_mtl(&mtl_path, &mut model.mtl_list, &mut name_list);
                    },
                    "usemtl" => {
                        let mtl_name = split[1].to_string();
                        // 絶対に mtl name が含まれていることが期待されるので、unwrap 失敗は panic
                        mtl_index = name_list.iter().enumerate().find(|s| s.1 == &mtl_name).unwrap().0;
                    }
                    "v" => {
                        let vs = line.split_whitespace().collect::<Vec<&str>>();
                        let v = Vec3::new(vs[1].parse::<f64>().unwrap(), vs[2].parse::<f64>().unwrap(), vs[3].parse::<f64>().unwrap());
                        v_lst.push(v);
                    },
                    "vt" => {
                        let vts = line.split_whitespace().collect::<Vec<&str>>();
                        let vt = Vec2::new(vts[1].parse::<f64>().unwrap(), vts[2].parse::<f64>().unwrap());
                        vt_lst.push(vt);
                    },
                    "vn" => {
                        let vns = line.split_whitespace().collect::<Vec<&str>>();
                        let vn = Vec3::new(vns[1].parse::<f64>().unwrap(), vns[2].parse::<f64>().unwrap(), vns[3].parse::<f64>().unwrap());
                        vn_lst.push(vn);
                    },
                    "f" => {
                        let mut fs = line.split_whitespace();
                        fs.next();
                        let mut indices = vec![];

                        for line in fs {
                            let vtx = line.split('/').collect::<Vec<&str>>();
                            // -1 : 1-indexed -> 0-indexed
                            let mut v_idx = vtx[0].parse::<i64>().unwrap_or(i64::max_value());
                            if v_idx < 0 { 
                                v_idx = v_lst.len() as i64 + v_idx;
                            } else {
                                v_idx -= 1;
                            }
                           
                            let mut vt_idx = if vtx.len() >= 2 {
                                let tmp = vtx[1].parse::<i64>().unwrap_or(i64::max_value());
                                if tmp < 0 {
                                    vt_lst.len() as i64 + tmp
                                } else {
                                    tmp - 1
                                }
                            } else {
                                i64::max_value()
                            };

                            let mut vn_idx = if vtx.len() >= 3 {
                                let tmp = vtx[2].parse::<i64>().unwrap_or(i64::max_value());
                                if tmp < 0 {
                                    vn_lst.len() as i64 + tmp
                                } else {
                                    tmp - 1
                                }
                            } else {
                                i64::max_value()
                            };

                            let v_idx = v_idx as usize;
                            let vt_idx = vt_idx as usize;
                            let vn_idx = vn_idx as usize;

                            let key = (v_idx, vt_idx, vn_idx);
                            let idx = if ptr2idx.contains_key(&key) {
                                *ptr2idx.get(&key).unwrap()
                            } else {
                                ptr2idx.insert(key, model.vertex_list.len());
                                model.vertex_list.push(v_lst[v_idx]);
                                if vt_idx < vt_lst.len() {
                                    model.texture_vertex_list.push(vt_lst[vt_idx]);
                                };
                                if vn_idx < vn_lst.len() {
                                    model.norm_list.push(vn_lst[vn_idx]);
                                }
                                model.vertex_list.len() - 1
                            };
                            indices.push(idx);
                        }

                        for i in 0..indices.len()-2 {
                            let d1 = model.vertex_list[indices[0]] - model.vertex_list[indices[i + 1]];
                            let d2 = model.vertex_list[indices[0]] - model.vertex_list[indices[i + 2]];
                            let norm = Vec3::cross(&d1, &d2).normalize();
                            
                            data_list.push(ModelFaceData::new(
                                [indices[0], indices[i + 1], indices[i + 2]],
                                mtl_index,
                                norm
                            ));
                        }
                    },
                    _ => {}
                }
            }
        }
        let obj_list = data_list.into_iter().enumerate().map(|(minor_id, item)| {
            ModelFace::new(item, &model.vertex_list, &model.texture_vertex_list)
        }).collect::<Vec<ModelFace>>();
        model.obj_manager.construct(obj_list);
        model
    }
}

impl Intersectable for ModelObject {
    fn bounding_box(&self) -> AABB {
        self._bounding_box
    }

    fn intersect(&self, ray: &Ray) -> Option<(&Object, &Primitive, f64)> {
        let tmp_ray = ray.translate_inv(self.scale, self.shift);

        match self.obj_manager.intersect(&tmp_ray) {
            Some((obj_ref, dist)) => Some((self, obj_ref, dist * self.scale)),
            None => None,
        }
    }
}

impl Object for ModelObject {
    fn material(&self, index: usize) -> &Material {
        &self.mtl_list[index]
    }
}

mod tests {

    use super::*;

    #[test]
    fn test_load_mdl() {
        let model_data = ModelObject::load_obj(r"data/sports_car/sportsCar.obj");
        assert_eq!(model_data.vertex_list.len(), 163634);
    }

    #[test]
    fn test_intersect_mdl() {
        let scale = 20f64;
        let offset = Vec3::new(1f64, 1f64, 1f64);
        let model_data = ModelObject::new("data/test/test.obj", scale, offset);
        let size = 100;
        let fsize = size as f64;
        for i in 0..size {
            for j in 0..size {
                let idx = i * size + j;
                let fi = i as f64;
                let fj = j as f64;
                let v1 = Vec3::new(fj / fsize, fi / fsize, 0f64) * scale + offset;
                let v2 = Vec3::new((fj + 1f64) / fsize, fi / fsize, 0f64) * scale + offset;
                let v3 = Vec3::new((fj + 1f64) / fsize, (fi + 1f64) / fsize, 0f64) * scale + offset;
                let v4 = Vec3::new(fj / fsize, (fi + 1f64) / fsize, 0f64) * scale + offset;
                let dir1 = ((v1 + v2 + v3) / 3f64).normalize();
                let ray1 = Ray::new(Vec3::zero(), dir1, 1f64);
                assert!(model_data.intersect(&ray1).is_some());

                let dir2 = ((v1 + v3 + v4) / 3f64).normalize();
                let ray2 = Ray::new(Vec3::zero(), dir2, 1f64);
                assert!(model_data.intersect(&ray2).is_some());
            }
        }
    }

    use test::Bencher;

    #[bench]
    fn bench_intersect(b: &mut Bencher) {
        let center = Vec3::new(10f64, 10f64, 10f64);
        let model_data = ModelObject::new("/home/xyz600/data/sports_car/sportsCar.obj", 10f64, center);
        let ray = Ray::new(
            Vec3::zero(),
            center.normalize(),
            1f64,
        );
        b.iter(|| {
            assert!(model_data.intersect(&ray).is_some());
        });
    }
}