use object::{Intersectable, Object, Material, Primitive};
use math::vec3::{Color, Vec3, Vec3x4};
use ray::Ray;
use common::{INF, EPS};
use brdf::ReflectionType;
use texture::Texture;
use ::random::Random;

use std::mem::swap;

use x86intrin::*;

#[derive(Clone, Copy, Debug)]
pub struct AABB {
    pub min: Vec3,
    pub max: Vec3,
}

impl AABB {
    pub fn new(_min: Vec3, _max: Vec3) -> AABB {
        AABB {
            min: _min,
            max: _max,
        }
    }

    pub fn union(&mut self, aabb: &AABB) {
        self.min = self.min.min(&aabb.min);
        self.max = self.max.max(&aabb.max);
    }

    pub fn init_for_union() -> AABB {
        AABB::new(
            Vec3::pos_inf(), Vec3::neg_inf()
        )
    }

    pub fn area(&self) -> f64 {
        let dx = self.max.x() - self.min.x();
        let dy = self.max.y() - self.min.y();
        let dz = self.max.z() - self.min.z();
        2f64 * (dx * dy + dx * dz + dy * dz)
    }

    pub fn intersect(&self, ray: &Ray) -> f64 {

        let mut g_tmin = 0f64;
        let mut g_tmax = INF;

        for axis in 0..3 {
            if ray.dir[axis] != 0f64 {
                let mut l_tmin = (self.min[axis] - ray.pos[axis]) / ray.dir[axis];
                let mut l_tmax = (self.max[axis] - ray.pos[axis]) / ray.dir[axis];
                if l_tmin > l_tmax {
                    std::mem::swap(&mut l_tmin, &mut l_tmax);
                }
                g_tmin = g_tmin.max(l_tmin);
                g_tmax = g_tmax.min(l_tmax);
                if g_tmin > g_tmax {
                    return INF;
                }
            }
        }
        if g_tmax < 0f64 {
            INF
        } else if g_tmin > 0f64 {
            g_tmin
        } else {
            g_tmax
        }
    }
}

pub struct Rectangle {
    pub shape: AABB,
    state: Material,
}

impl Rectangle {
    pub fn new(_shape: AABB, _state: Material) -> Rectangle {
        Rectangle {
            shape: _shape,
            state: _state,
        }
    }
}

impl Object for Rectangle {
    fn material(&self, _: usize) -> &Material {
        &self.state
    }
}

impl Intersectable for Rectangle {
    fn bounding_box(&self) -> AABB {
        AABB::new(self.shape.min, self.shape.max)
    }

    fn intersect(&self, ray: &Ray) -> Option<(&Object, &Primitive, f64)> {
        let t = self.shape.intersect(ray);
        if t > EPS {
            Some((self, self, t))
        } else {
            None
        }
    }
}

impl Primitive for Rectangle {
    fn normal(&self, pos: Vec3) -> Vec3 {
        if (pos.x() - self.shape.min.x()).abs() < EPS || (pos.x() - self.shape.max.x()).abs() < EPS {
            Vec3::new(1f64, 0f64, 0f64)
        } else if (pos.y() - self.shape.min.y()).abs() < EPS || (pos.y() - self.shape.max.y()).abs() < EPS {
            Vec3::new(0f64, 1f64, 0f64)
        } else if (pos.z() - self.shape.min.z()).abs() < EPS || (pos.z() - self.shape.max.z()).abs() < EPS {
            Vec3::new(0f64, 0f64, 1f64)
        } else {
            Vec3::new(-1f64, -1f64, -1f64)
        }
    }
}

#[derive(Debug)]
pub struct AABBx4 {
    min: Vec3x4,
    max: Vec3x4,
}

impl AABBx4 {
    pub fn new(b0: AABB, b1: AABB, b2: AABB, b3: AABB) -> AABBx4 {
        AABBx4 {
            min: Vec3x4::new(b0.min, b1.min, b2.min, b3.min),
            max: Vec3x4::new(b0.max, b1.max, b2.max, b3.max)
        }
    }

    pub fn init_for_union() -> AABBx4 {
        AABBx4 {
            min: Vec3x4::new(Vec3::pos_inf(), Vec3::pos_inf(), Vec3::pos_inf(), Vec3::pos_inf()),
            max: Vec3x4::new(Vec3::neg_inf(), Vec3::neg_inf(), Vec3::neg_inf(), Vec3::neg_inf())
        }
    }
}

impl AABBx4 {

    fn update(pos: f64, dir: f64, mins: m256d, maxs: m256d, g_tmin: m256d, g_tmax: m256d, invalid_mask: m256d) -> (m256d, m256d, m256d){
        let pos4 = mm256_set1_pd(pos);
        let dir4 = mm256_set1_pd(dir);

        let _l_tmin = mm256_div_pd(mm256_sub_pd(mins, pos4), dir4);
        let _l_tmax = mm256_div_pd(mm256_sub_pd(maxs, pos4), dir4);

        let l_tmin = mm256_min_pd(_l_tmin, _l_tmax);
        let l_tmax = mm256_max_pd(_l_tmin, _l_tmax);

        let next_g_tmin = mm256_max_pd(g_tmin, l_tmin);
        let next_g_tmax = mm256_min_pd(g_tmax, l_tmax);

        let cmp_g = mm256_cmp_pd(next_g_tmin, next_g_tmax, CMP_GE_OQ);
        let next_invalid_mask = mm256_or_pd(invalid_mask, cmp_g);

        (next_g_tmin, next_g_tmax, next_invalid_mask)
    }

    pub fn intersect(&self, ray: &Ray) -> [f64; 4] {
        let g_tmin = mm256_set1_pd(0f64);
        let g_tmax = mm256_set1_pd(INF);
        let invalid_mask = mm256_set1_epi64x(0).as_m256d();

        let (g_tmin, g_tmax, invalid_mask) = if ray.dir.x() == 0f64 {
            (g_tmin, g_tmax, invalid_mask)
        } else {
            let min_xs = self.min.xs.as_m256d();
            let max_xs = self.max.xs.as_m256d();
            AABBx4::update(ray.pos.x(), ray.dir.x(), min_xs, max_xs, g_tmin, g_tmax, invalid_mask)
        };

        if mm256_movemask_pd(invalid_mask) == 0xfi32 {
            return [INF, INF, INF, INF];
        }

        let (g_tmin, g_tmax, invalid_mask) = if ray.dir.y() == 0f64 {
            (g_tmin, g_tmax, invalid_mask)
        } else {
            let min_ys = self.min.ys.as_m256d();
            let max_ys = self.max.ys.as_m256d();
            AABBx4::update(ray.pos.y(), ray.dir.y(), min_ys, max_ys, g_tmin, g_tmax, invalid_mask)
        };

        if mm256_movemask_pd(invalid_mask) == 0xfi32 {
            return [INF, INF, INF, INF];
        }

        let (g_tmin, g_tmax, invalid_mask) = if ray.dir.z() == 0f64 {
            (g_tmin, g_tmax, invalid_mask)
        } else {
            let min_zs = self.min.zs.as_m256d();
            let max_zs = self.max.zs.as_m256d();
            AABBx4::update(ray.pos.z(), ray.dir.z(), min_zs, max_zs, g_tmin, g_tmax, invalid_mask)
        };

        if mm256_movemask_pd(invalid_mask) == 0xfi32 {
            return [INF, INF, INF, INF];
        }

        let zeros = mm256_set1_pd(0f64);
        let g_tmin_pos_mask = mm256_cmp_pd(g_tmin, zeros, CMP_GT_OQ);
        let g_tmin_pos_mask_imm = mm256_movemask_pd(g_tmin_pos_mask);

        let ans = mm256_blend_pd(g_tmax, g_tmin, g_tmin_pos_mask_imm);
        
        let g_tmax_neg_mask = mm256_cmp_pd(g_tmax, zeros, CMP_LT_OQ);
        let invalid_mask = mm256_or_pd(invalid_mask, g_tmax_neg_mask);
        let invalid_mask_imm = mm256_movemask_pd(invalid_mask);

        let invalid_ans = mm256_set1_pd(INF);

        mm256_blend_pd(ans, invalid_ans, invalid_mask_imm).as_f64x4().as_array()
    }
}

#[cfg(test)]
mod rect_tests {
    
    use common::EPS;
    use object::rectangle::{AABB, AABBx4};
    use math::vec3::{Vec3, Vec3x4};
    use ray::Ray;

    use x86intrin::*;

    #[test]
    fn test_intersect () {
        let aabb = AABB::new(Vec3::new(1f64, 1f64, 1f64), Vec3::new(2f64, 2f64, 2f64));
        let ray = Ray::new(Vec3::new(0f64, 0f64, 0f64), Vec3::new(1f64, 1f64, 1f64).normalize(), 1f64);

        let t = aabb.intersect(&ray);

        assert!((3f64.sqrt() - t).abs() < EPS);

        let aabb = AABB::new(Vec3::new(0f64, 1f64, 0f64), Vec3::new(1f64, 2f64, 1f64));
        let ray = Ray::new(Vec3::new(0f64, 0f64, 0f64), Vec3::new(1f64, 2f64, 0f64).normalize(), 1f64);

        let t = aabb.intersect(&ray);

        assert!((5f64.sqrt() / 2f64 - t).abs() < EPS);
    }

    #[test]
    fn test_union() {
        let mut ab1 = AABB::new(Vec3::new(0f64, 1f64, 2f64), Vec3::new(2f64, 3f64, 4f64));
        let ab2 = AABB::new(Vec3::new(-2f64, 2f64, 1f64), Vec3::new(5f64, 4f64, 3f64));
        ab1.union(&ab2);
        assert_eq!(ab1.min, Vec3::new(-2f64, 1f64, 1f64));
        assert_eq!(ab1.max, Vec3::new(5f64, 4f64, 4f64));
    }

    #[test]
    fn test_intersectx4() {
        let boxs = AABBx4::new(
            AABB::new(Vec3::new(0f64, 0f64, 0f64), Vec3::new(1f64, 1f64, 1f64)),
            AABB::new(Vec3::new(1f64, 1f64, 1f64), Vec3::new(2f64, 2f64, 2f64)),
            AABB::new(Vec3::new(2f64, 2f64, 2f64), Vec3::new(3f64, 3f64, 3f64)),
            AABB::new(Vec3::new(3f64, 3f64, 3f64), Vec3::new(4f64, 4f64, 4f64)),
        );
        let ray = Ray::new(
            Vec3::new(-1f64, -1f64, -1f64),
            Vec3::new(1f64, 1f64, 1f64).normalize(),
            1f64
        );
        let result = boxs.intersect(&ray);
        println!("{:?}", &result);
        assert!((result[0] - 3f64.sqrt()).abs() < EPS);
        assert!((result[1] - 12f64.sqrt()).abs() < EPS);
        assert!((result[2] - 27f64.sqrt()).abs() < EPS);
        assert!((result[3] - 48f64.sqrt()).abs() < EPS);
    }

    #[test]
    fn test_random_intersect() {
        let mut rand = Random::from_seed(0);

        for iter in 0..1000 {
            let mut AABBs = vec![];
            for i in 0..4 {
                let v1 = Vec3::new(rand.gen_range(0f64, 1f64), rand.gen_range(0f64, 1f64), rand.gen_range(0f64, 1f64));
                let v2 = Vec3::new(rand.gen_range(0f64, 1f64), rand.gen_range(0f64, 1f64), rand.gen_range(0f64, 1f64));

                let min_x = v1.min(&v2);
                let max_x = v1.max(&v2);
                let bb = AABB::new(min_x, max_x);
                AABBs.push(bb);
            }
            let mut ts = [0f64; 4];
            let ray = Ray::new(Vec3::new(0f64, 0f64, 0f64), Vec3::new(1f64, 1f64, 1f64).normalize(), 1f64);
            for i in 0..4 {
                ts[i] = AABBs[i].intersect(&ray);
            }

            let abx4 = AABBx4::new(AABBs[0], AABBs[1], AABBs[2], AABBs[3]);
            let tsx4 = abx4.intersect(&ray);

            for i in 0..4 {
                let diff = (ts[i] - tsx4[i]).abs();
                if diff > EPS {
                    println!("i = {}", i);
                    println!("{:?}", AABBs[i]);
                    println!("{:?}", ray);
                    println!("(exp, act) = ({}, {})", ts[i], tsx4[i]);
                    panic!();
                    println!("fail!");
                } else {
                    println!("success!");
                }
            }
        }
    }

    use test::Bencher;
    use super::*;
    extern crate test;

    #[bench]
    fn bench_intersect(b: &mut Bencher) {
        let boxs = AABB::new(
            Vec3::new(0f64, 0f64, 0f64),
            Vec3::new(1f64, 1f64, 1f64)
        );
        let ray = Ray::new(
            Vec3::new(-1f64, -1f64, -1f64),
            Vec3::new(1f64, 1f64, 1f64).normalize(),
            1f64,
        );
        b.iter(|| {
            let _ray = test::black_box(&ray);
            assert!(boxs.intersect(&_ray) < INF);
        });
    }

    #[bench]
    fn bench_intersectx4(b: &mut Bencher) {
        let boxs = AABBx4::new(
            AABB::new(Vec3::new(0f64, 0f64, 0f64), Vec3::new(1f64, 1f64, 1f64)),
            AABB::new(Vec3::new(1f64, 1f64, 1f64), Vec3::new(2f64, 2f64, 2f64)),
            AABB::new(Vec3::new(2f64, 2f64, 2f64), Vec3::new(3f64, 3f64, 3f64)),
            AABB::new(Vec3::new(3f64, 3f64, 3f64), Vec3::new(4f64, 4f64, 4f64)),
        );
        let ray = Ray::new(
            Vec3::new(-1f64, -1f64, -1f64),
            Vec3::new(1f64, 1f64, 1f64).normalize(),
            1f64
        );
        b.iter(|| {
            let _ray = test::black_box(&ray);
            boxs.intersect(&_ray);
        });
    }
}