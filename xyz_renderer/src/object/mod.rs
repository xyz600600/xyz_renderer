use ::math::vec3::{Vec3, Color};
use ::brdf::ReflectionType;
use ::texture::Texture;
use ::texture::image_texture::ImageTexture;
use ::texture::monotone_texture::MonotoneTexture;
use ::image::Image;
use ::ray::Ray;
use ::random::Random;

use ::object::rectangle::AABB;

use ::std::path::Path;
use ::std::convert::Into;

pub struct Material {
    pub texture: Box<Texture>, 
    pub brdf: ReflectionType,
    pub emission: Color,
}

impl Material {
    pub fn new(_texture: Box<Texture>, _brdf: ReflectionType, _emission: Color) -> Material
    {
        Material
        {
            texture: _texture,
            brdf: _brdf,
            emission: _emission
        }
    }
}

struct MaterialState {
    name: String,
    ka: Color,
    kd: Color,
    ks: Color, 
    tf: Color,
    d: f64,
    ns: f64,
    ni: f64,
    map_kd: String,
    map_ke: String,
}

impl Into<Material> for MaterialState {
    
    // TODO: 間違っているので、いつか修正する
    fn into(self) -> Material {
        let select = |v: &Vec3, th: f64| v.norm() > th;
        let use_diffuse = select(&self.kd, 0f64);
        let use_specular = select(&self.ks, 0f64);
        let use_refraction = self.ni > 1f64;
        let mut color = self.kd;

        let ref_type = if use_diffuse && !use_specular && !use_refraction {
            color = self.kd;
            ReflectionType::Diffuse
        } else if !use_diffuse && use_specular && !use_refraction {
            color = self.ks;
            ReflectionType::Specular
        } else if !use_diffuse && !use_specular && use_refraction {
            color = Vec3::zero() + (self.ni - 1f64);
            ReflectionType::Refraction(self.ni)
        } else {
            color = self.kd.max(&self.ks).max(&(Vec3::zero() + (self.ni - 1f64)));
            ReflectionType::Mixture(self.kd.max_elements(), self.ks.max_elements(), self.tf.max_elements() - 1f64, self.ni)
        };

        let texture: Box<Texture> = if self.map_kd.len() == 0 {
            Box::new(MonotoneTexture::new(color))
        } else {
            Box::new(ImageTexture::new(Image::load(Path::new(&self.map_kd))))
        };

        // TODO: model object の emission 対応
        Material::new(texture, ref_type, Color::zero())
    }
}

impl MaterialState {

    pub fn new() -> MaterialState {
        MaterialState {
            name: "".to_string(),
            ka: Color::zero(),
            kd: Color::zero(),
            ks: Color::zero(),
            tf: Color::zero(),
            d: 0f64,
            ns: 0f64,
            ni: 1f64,
            map_kd: String::new(),
            map_ke: String::new(),
        }
    }
}

pub trait Intersectable : Sync + Send {

    // (obj_ref, distance, primitive_id)
    fn intersect(&self, ray: &Ray) -> Option<(&Object, &Primitive, f64)>;

    fn bounding_box(&self) -> AABB;
}

pub trait Object : Intersectable {

    fn material(&self, index: usize) -> &Material;

    fn light(&self) -> Option<&Primitive> {
        None
    }

    fn sample(&self, random: &mut Random) -> (f64, Ray) {
        unimplemented!();
    }
}

pub trait Primitive : Sync + Send {

    fn normal(&self, pos: Vec3) -> Vec3;

    fn material_id(&self) -> usize {
        0
    }
}

pub mod sphere;
pub mod rectangle;
pub mod polygon;
pub mod model_object;