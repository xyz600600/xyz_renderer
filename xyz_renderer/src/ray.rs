use math::vec3::{Vec3};

use x86intrin::*;

#[derive(Debug, Copy, Clone)]
pub struct Ray {
    pub pos: Vec3,
    pub dir: Vec3,
    pub ni: f64,
}

impl Ray {
    pub fn new(_pos: Vec3, _dir: Vec3, _ni: f64) -> Ray {
        Ray {
            pos: _pos,
            dir: _dir,
            ni: _ni,
        }
    }

    pub fn translate_inv(&self, scale: f64, shift: Vec3) -> Ray {
        Ray::new((self.pos - shift) / scale, self.dir, self.ni)
    }
}