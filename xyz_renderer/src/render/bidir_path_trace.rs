use ::object_manager::linear_object_manager::LinearObjectManager;
use ::object::{Intersectable};
use ::ray::Ray;
use ::math::vec3::{Vec3, Color};
use ::common::INF;
use ::random::Random;
use ::brdf::ReflectionType;
use ::camera::Camera;

const MAX_DEPTH: usize = 20;

#[derive(Debug, Copy, Clone)]
struct Vertex {
    ray: Ray,
    norm: Vec3,
    emit: Color,
    dist: f64,
    pdf: f64,
    throughput: Color,
}

impl Vertex {
    fn new() -> Vertex {
        Vertex {
            ray: Ray::new(Vec3::zero(), Vec3::zero(), 1f64),
            norm: Vec3::zero(),
            emit: Color::zero(),
            dist: 0f64,
            pdf: 1f64,
            throughput: Color::new(1f64, 1f64, 1f64),
        }
    }
}

/// TODO: レンズへの Hit を考慮できていない
fn sample(ray: Ray, obj_manager: &LinearObjectManager, rand: &mut Random, vertex_list: &mut [Vertex; MAX_DEPTH]) -> usize {

    // 常に Hit したオブジェクトを中心に考える
    let mut depth = 1;
    vertex_list[0].ray = ray;

    while let (dist, Some(obj)) = obj_manager.intersect(&vertex_list[depth - 1].ray) {

        vertex_list[depth - 1].dist = dist;
        vertex_list[depth].emit = obj.emission();
        let color = obj.color();

        let russian_prob = if depth <= 5 {
            1f64
        } else {
            color.x().max(color.y()).max(color.z()) * 0.5f64.powi((depth - 5) as i32)
        };
        if rand.gen_range(0f64, 1f64) >= russian_prob {
            depth += 1;
            break;
        }
        vertex_list[depth] *= russian_prob;
        
        let next_pos = vertex_list[depth - 1].ray.pos + vertex_list[depth - 1].ray.dir * dist;
        vertex_list[depth].norm = obj.normal(next_pos);
        if Vec3::dot(&vertex_list[depth].norm, &vertex_list[depth - 1].ray.dir) > 0f64 {
            vertex_list[depth].norm = -vertex_list[depth].norm;
        }

        // 立体角の pdf である点に注意
        let (pdf, orig_pdf, next_ray) = ReflectionType::next_ray(obj.brdf(), &vertex_list[depth - 1].ray, next_pos, vertex_list[depth].norm, rand);

        vertex_list[depth + 1].pdf *= orig_pdf;
        vertex_list[depth + 1].throughput *= color * (pdf * Vec3::dot(&next_ray.dir, &vertex_list[depth + 1].norm));

        depth += 1;
        vertex_list[depth].ray = next_ray;
    }
    depth
}

pub fn radiance(y: usize, x: usize, camera: &Camera, obj_manager: &LinearObjectManager, _: usize, rand: &mut Random) -> Color {

    let mut camera_vertex_list = [Vertex::new(); MAX_DEPTH];
    let (pdf, camera_ray) = camera.get_initial_ray(y, x, rand);
    let depth_camera = sample(camera_ray, &obj_manager, rand, &mut camera_vertex_list);

    // camera path (v = 0) の特殊な pdf を埋める
    camera_vertex_list[0].norm = camera.camera_dir();
    camera_vertex_list[0].color = Color::new(1f64, 1f64, 1f64);
    camera_vertex_list[0].real_pdf = 

    // real_pdf と orig_pdf を、全て幾何項で計算する
    // real_pdf と throughput の累積値を全て埋める

    let mut light_vertex_list = [Vertex::new(); MAX_DEPTH];
    let light_ray = obj_manager.light().sample(rand);
    let depth_light = sample(light_ray, &obj_manager, rand, &mut light_vertex_list);

    // light path の特殊な pdf を埋める
    // real_pdf と orig_pdf を、全て幾何項で計算する
    // reaal_pdf と throughput の累積値を全て埋める

    camera_vertex_list[0].emit
}