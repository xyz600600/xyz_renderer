use ::environment::Environment;
use ::image::{Image, SubImage};
use ::random::Random;
use ::camera::{Camera};
use ::ray::Ray;
use ::math::vec3::{Color, Vec3};

use std::path::Path;

use rayon::prelude::*;

pub mod path_trace;
pub mod path_trace_nee;
// pub mod bidir_path_trace;

pub fn render(env: Environment) {

    let mut img = Image::new(env.resolution_y, env.resolution_x);
    let mut rand = Random::from_seed(0);

    for y in 0..env.resolution_y {
        println!("y = {}", y);
        for x in 0..env.resolution_x {
            for _ in 0..env.sample_per_pixel {
                img[y][x] += path_trace_nee::radiance(y, x, &env, 0usize, &mut rand);
            }
            img[y][x] /= env.sample_per_pixel as f64;
        }
    };
    img.save(Path::new(&env.output_path));
}

pub fn render_parallel(env: Environment) {
    let mut subimg_array = SubImage::create_grid_subimage_list(env.resolution_y, env.resolution_x, env.grid_size);
    let all_num = subimg_array.len();

    subimg_array.par_iter_mut().for_each(|sub_img| {
        let mut rand = Random::from_seed(sub_img.sx * sub_img.sy);
        for dy in 0..sub_img.height {
            let y = sub_img.sy + dy;
            for dx in 0..sub_img.width {
                let x = sub_img.sx + dx;
                for _ in 0..env.sample_per_pixel {
                    sub_img[dy][dx] += path_trace_nee::radiance(y, x, &env, 0usize, &mut rand);
                }
            }
        };
        println!("some / {} finished.", all_num);
    });
    let mut img = Image::new(env.resolution_y, env.resolution_x);
    for subimg in subimg_array {
        subimg.update_to(&mut img, env.sample_per_pixel);
    }
    img.save(Path::new(&env.output_path));
}

pub fn render_parallel_prod(env: Environment) {

    let mut img = Image::new(env.resolution_y, env.resolution_x);
    let mut subimg_array = SubImage::create_grid_subimage_list(env.resolution_y, env.resolution_x, env.grid_size);

    const TIME_LIMIT: u64 = 120;
    const OUTPUT_STEP_SEC: u64 = 15;

    let mut output_trig_sec = OUTPUT_STEP_SEC;
    let mut current_spp = 0;
    let mut index = 0;

    loop {
        subimg_array.par_iter_mut().for_each(|sub_img| {

            let mut rand = Random::from_seed((sub_img.sx + 1) * (sub_img.sy + 1) * current_spp);

            for dy in 0..sub_img.height {
                let y = sub_img.sy + dy;
                for dx in 0..sub_img.width {
                    let x = sub_img.sx + dx;
                    sub_img[dy][dx] += path_trace_nee::radiance(y, x, &env, 0usize, &mut rand);
                }
            };
        });

        current_spp += 1;
        println!("{} spp finished.", current_spp);

        let time = env.time.elapsed().as_secs();
        if time > output_trig_sec {
            for sub_img in &subimg_array {
                sub_img.update_to(&mut img, current_spp);
            }
            let path_str = format!("result_{:<03}.png", index);
            index += 1;
            img.save(Path::new(&path_str));
            println!("image saved");
            output_trig_sec += OUTPUT_STEP_SEC;
        }
        if time > TIME_LIMIT {
            break;
        }
    }
    img.save(Path::new(&env.output_path));
}
