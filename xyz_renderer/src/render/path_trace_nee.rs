use ::object_manager::linear_object_manager::LinearObjectManager;
use ::object::{Intersectable, Object, Primitive};
use ::ray::Ray;
use ::math::vec3::{Vec3, Color};
use ::common::{EPS, INF};
use ::random::Random;
use ::brdf::ReflectionType;
use ::camera::Camera;
use ::environment::Environment;

#[derive(Debug, Copy, Clone)]
struct Vertex {
    ray: Ray,
    norm: Vec3,
    color: Color,
    emit: Color,
    weight: f64,
    dist: f64,
}

impl Vertex {
    fn new() -> Vertex {
        Vertex {
            ray: Ray::new(Vec3::zero(), Vec3::zero(), 1f64),
            norm: Vec3::zero(),
            color: Color::zero(),
            emit: Color::zero(),
            weight: 0f64,
            dist: 0f64,
        }
    }
}

pub fn radiance(y: usize, x: usize, environment: &Environment, _: usize, rand: &mut Random) -> Color {
    let (weight, ray) = environment.camera.get_initial_ray(y, x, rand);
    _radiance_recur(ray, &environment, 0, rand) * weight
}

fn _radiance_recur(ray: Ray, environment: &Environment, depth: usize, rand: &mut Random) -> Color {

    if let Some((obj, prim, dist)) = environment.obj_manager.intersect(&ray) {

        let material = obj.material(prim.material_id());
        let color = material.texture.color();

        let russian_prob = if depth <= 5 {
            1f64
        } else {
            color.max_elements() * 0.5f64.powi((depth - 5) as i32)
        };

        if color.max_elements() == 0f64 || rand.gen_range(0f64, 1f64) >= russian_prob {
            material.emission
        } else {
            let next_pos = ray.pos + ray.dir * dist;
            let normal = prim.normal(next_pos);
            let normal = if Vec3::dot(&normal, &ray.dir) < 0f64 {
                normal
            } else {
                -normal
            };

            let brdf = material.brdf;
            let (cont, cont_orig, next_ray) = ReflectionType::next_ray(brdf, &ray, next_pos, normal, rand);
            let mut ret = material.emission * cont;
            let mut prob_sum = cont;

            if material.emission.max_elements() == 0f64 {
                // Next Event Estimation
                // light_sample には、光源に対する normal が入っている
                let (light_weight, light_sample) = environment.light(0).sample(rand);
                let light_ray = Ray::new(next_pos, (light_sample.pos - next_pos).normalize(), ray.ni);

                if let Some((light_obj, light_prim, light_dist)) = environment.obj_manager.intersect(&light_ray) {
                    let expected_dist = (next_pos - light_sample.pos).norm();
                    if (expected_dist - light_dist).abs() < EPS * 1e2 {
                        let prob = ReflectionType::get_brdf(brdf, &ray, &light_ray, &normal);
                        if prob > 0f64 {
                            let g = (Vec3::dot(&light_ray.dir, &light_sample.dir) * Vec3::dot(&light_ray.dir, &normal) / light_dist.powi(2)).abs();
                            let additional = light_obj.material(light_prim.material_id()).emission * color * (g * prob / light_weight);
                            ret += additional * prob;
                            prob_sum += prob;
                        }
                    }
                }
            }
            ret /= prob_sum;

            let contrib = color * ((Vec3::dot(&ray.dir, &normal) / russian_prob)).abs();
            let next_radiance = _radiance_recur(next_ray, &environment, depth + 1usize, rand);

            ret + next_radiance * contrib
        }
    } else {
        Color::zero()
    }
}