use ::object_manager::linear_object_manager::LinearObjectManager;
use ::object::{Intersectable};
use ::ray::Ray;
use ::math::vec3::{Vec3, Color};
use ::common::INF;
use ::random::Random;
use ::brdf::ReflectionType;
use ::camera::Camera;
use ::environment::Environment;

#[derive(Debug, Copy, Clone)]
struct Vertex {
    ray: Ray,
    norm: Vec3,
    color: Color,
    emit: Color,
    weight: f64,
    dist: f64,
}

impl Vertex {
    fn new() -> Vertex {
        Vertex {
            ray: Ray::new(Vec3::zero(), Vec3::zero(), 1f64),
            norm: Vec3::zero(),
            color: Color::zero(),
            emit: Color::zero(),
            weight: 0f64,
            dist: 0f64,
        }
    }
}

pub fn test(y: usize, x: usize, environment: &Environment, depth: usize, rand: &mut Random) -> Color {

    let (_, ray) = environment.camera.get_initial_ray(y, x, rand);

    if let Some((obj, prim, dist)) = environment.obj_manager.intersect(&ray) {

        let pos = ray.pos + ray.dir * dist;
        let d = (dist / 200f64).powi(3); 
        
        // return Vec3::new(d, d, d);
        // return Vec3::dot(&obj.normal(pos), &ray.dir).abs();

        let material = obj.material(prim.material_id());
        let color = material.texture.color();
        // return color;

        let russian_prob = if depth <= 5 {
            1f64
        } else {
            color.x().max(color.y()).max(color.z()) * 0.5f64.powi((depth - 5) as i32)
        };

        if color.max_elements() == 0f64 || rand.gen_range(0f64, 1f64) >= russian_prob {
            material.emission
        } else {

            let next_pos = ray.pos + ray.dir * dist;
            let normal = prim.normal(next_pos);
            let normal = if Vec3::dot(&normal, &ray.dir) < 0f64 {
                normal
            } else {
                -normal
            };
            return normal.abs();

            let (cont, cont_orig, next_ray) = ReflectionType::next_ray(material.brdf, &ray, next_pos, normal, rand);
            let contrib = color * ((Vec3::dot(&ray.dir, &normal) / russian_prob)).abs();
            next_ray.dir.abs()
        }
    } else {
        Color::zero()
    }
}

pub fn radiance(y: usize, x: usize, environment: &Environment, _: usize, rand: &mut Random) -> Color {
    let (weight, ray) = environment.camera.get_initial_ray(y, x, rand);
    _radiance_recur(ray, &environment, 0, rand) * weight
}

fn _radiance_recur(ray: Ray, environment: &Environment, depth: usize, rand: &mut Random) -> Color {

    if let Some((obj, prim, dist)) = environment.obj_manager.intersect(&ray) {
        
        let material = obj.material(prim.material_id());
        let color = material.texture.color();

        let russian_prob = if depth <= 5 {
            1f64
        } else {
            color.x().max(color.y()).max(color.z()) * 0.5f64.powi((depth - 5) as i32)
        };

        if color.max_elements() == 0f64 || rand.gen_range(0f64, 1f64) >= russian_prob {
            material.emission
        } else {

            let next_pos = ray.pos + ray.dir * dist;
            let normal = prim.normal(next_pos);
            let normal = if Vec3::dot(&normal, &ray.dir) < 0f64 {
                normal
            } else {
                -normal
            };

            let (cont, cont_orig, next_ray) = ReflectionType::next_ray(material.brdf, &ray, next_pos, normal, rand);
            let contrib = color * ((Vec3::dot(&ray.dir, &normal) / russian_prob)).abs();

            let next_radiance = _radiance_recur(next_ray, &environment, depth + 1usize, rand);

            material.emission + next_radiance * contrib
        }
    } else {
        Color::zero()
    }
}

pub fn radiance_norecur(y: usize, x: usize, environment: &Environment, _: usize, rand: &mut Random) -> Color {

    let (weight, ray) = environment.camera.get_initial_ray(y, x, rand);

    const MAX_DEPTH: usize = 20;
    let mut vertex_list = [Vertex::new(); MAX_DEPTH];
    let mut depth = 0;
    vertex_list[depth].ray = ray;

    while let Some((obj, prim, dist)) = environment.obj_manager.intersect(&vertex_list[depth].ray) {

        let mut last = vertex_list[depth];

        let material = obj.material(prim.material_id());
        last.dist = dist;
        last.color = material.texture.color();
        last.emit = material.emission;
        let russian_prob = if depth <= 5 {
            1f64
        } else {
            last.color.x().max(last.color.y()).max(last.color.z()) * 0.5f64.powi((depth - 5) as i32)
        };
        if rand.gen_range(0f64, 1f64) >= russian_prob {
            break;
        }
        let next_pos = last.ray.pos + last.ray.dir * dist;
        last.norm = prim.normal(next_pos);
        if Vec3::dot(&last.norm, &last.ray.dir) > 0f64 {
            last.norm = -last.norm;
        }
        let (cont, cont_orig, next_ray) = ReflectionType::next_ray(material.brdf, &last.ray, next_pos, last.norm, rand);
        last.weight = ((Vec3::dot(&last.ray.dir, &last.norm) / russian_prob)).abs();
        vertex_list[depth] = last;

        depth += 1;
        vertex_list[depth].ray = next_ray;
    }

    for index in (1..depth+1).rev() {
        vertex_list[index - 1].emit += vertex_list[index].emit * vertex_list[index - 1].color * vertex_list[index - 1].weight;
    }
    vertex_list[0].emit * weight
}